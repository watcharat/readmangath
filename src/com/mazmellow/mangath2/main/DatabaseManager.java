package com.mazmellow.mangath2.main;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseManager extends SQLiteOpenHelper {

	Context context;

	// Database Version
	private static final int DATABASE_VERSION = 2;

	// Database Name
	private static final String DATABASE_NAME = "readmangath.db";

	// Table Name
	private static final String TABLE_BOOKMARK = "bookmarks";
	private static final String TABLE_CHAPTER_READ = "chapter_reads";

	// -------VERSION 1-------------
	// ---bookmarks---
	private static final String COL_TITLE = "title";
	private static final String COL_DESC = "desc";
	private static final String COL_IMG = "img";
	private static final String COL_URL = "url";
	// -------------------------------

	public DatabaseManager(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
		this.context = context;

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		System.out.println("------ onCreate --------");
		try {
			db.execSQL("CREATE TABLE " + TABLE_BOOKMARK
					+ "("
					// BookmarkID INTEGER PRIMARY KEY,"
					+ " " + COL_TITLE + " TEXT(100)," + " " + COL_DESC
					+ " TEXT(100)," + " " + COL_IMG + " TEXT(100)," + " "
					+ COL_URL + " TEXT(100));");

			Log.d("CREATE TABLE", "Create Table Successfully.");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("DB ERROR: " + e);
		}

		createTableRead(db);
	}

	private void createTableRead(SQLiteDatabase db) {
		System.out.println("--- createTableRead ---");
		try {
			db.execSQL("CREATE TABLE " + TABLE_CHAPTER_READ + "(" + " "
					+ COL_TITLE + " TEXT(100)," + " " + COL_DESC
					+ " TEXT(100),"
					+ " " + COL_URL + " TEXT(100));");

			Log.d("CREATE TABLE", "Create Table Successfully.");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("DB ERROR: " + e);
		}
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		super.onOpen(db);
		System.out.println("------ onOpen --------");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		System.out.println("------ onUpgrade --------");
		System.out.println("------ oldVersion : " + oldVersion);
		System.out.println("------ newVersion : " + newVersion);

		if (oldVersion == 1 && newVersion == 2) {
			createTableRead(db);
		}
	}

	// Insert Data
	public boolean insertOrUpdateBookmark(String title, String desc,
			String img, String url) {
		// TODO Auto-generated method stub

		if (getBookmarkByTitle(title) == null) {
			try {
				SQLiteDatabase db = this.getWritableDatabase();
				/**
				 * for API 11 and above SQLiteStatement insertCmd; String strSQL
				 * = "INSERT INTO " + TABLE_MEMBER +
				 * "(MemberID,Name,Tel) VALUES (?,?,?)";
				 * 
				 * insertCmd = db.compileStatement(strSQL);
				 * insertCmd.bindString(1, strMemberID); insertCmd.bindString(2,
				 * strName); insertCmd.bindString(3, strTel); return
				 * insertCmd.executeInsert();
				 */

				ContentValues Val = new ContentValues();
				Val.put(COL_TITLE, title);
				Val.put(COL_DESC, desc);
				Val.put(COL_IMG, img);
				Val.put(COL_URL, url);

				db.insert(TABLE_BOOKMARK, null, Val);

				if (db != null) {
					db.close();
				}

				return true; // return rows inserted.

			} catch (Exception e) {
				Log.i("insertOrUpdateBookmark", e.getMessage());
				return false;
			}
		} else {
			return true;
		}

	}

	public boolean insertOrUpdateChapterRead(String title, String desc, String url) {
		// TODO Auto-generated method stub

		if (getChapterReadByUrl(url) == null) {
			try {
				SQLiteDatabase db = this.getWritableDatabase();
				/**
				 * for API 11 and above SQLiteStatement insertCmd; String strSQL
				 * = "INSERT INTO " + TABLE_MEMBER +
				 * "(MemberID,Name,Tel) VALUES (?,?,?)";
				 * 
				 * insertCmd = db.compileStatement(strSQL);
				 * insertCmd.bindString(1, strMemberID); insertCmd.bindString(2,
				 * strName); insertCmd.bindString(3, strTel); return
				 * insertCmd.executeInsert();
				 */

				ContentValues Val = new ContentValues();
				Val.put(COL_TITLE, title);
				Val.put(COL_DESC, desc);
				Val.put(COL_URL, url);

				db.insert(TABLE_CHAPTER_READ, null, Val);

				if (db != null) {
					db.close();
				}

				return true; // return rows inserted.

			} catch (Exception e) {
				Log.i("insertOrUpdateChapterRead", e.getMessage());
				return false;
			}
		} else {
			return true;
		}

	}

	public HashMap<String, String> getBookmarkByTitle(String title) {
		// TODO Auto-generated method stub
		HashMap<String, String> hash = null;
		try {
			// SQLiteDatabase db;
			SQLiteDatabase db = this.getWritableDatabase(); // Read Data

			String strSQL = "SELECT * FROM " + TABLE_BOOKMARK + " WHERE "
					+ COL_TITLE + " = '" + title + "'";
			Cursor cursor = db.rawQuery(strSQL, null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					hash = new HashMap<String, String>();
					hash.put(COL_TITLE, cursor.getString(0));
					hash.put(COL_DESC, cursor.getString(1));
					hash.put(COL_IMG, cursor.getString(2));
					hash.put(COL_URL, cursor.getString(3));
				}
			}
			cursor.close();
			if (db != null) {
				db.close();
			}
			return hash;

		} catch (Exception e) {
			Log.i("getBookmarkByTitle", e.getMessage());
			return null;
		}

	}
	
	public HashMap<String, String> getChapterReadByUrl(String url) {
		// TODO Auto-generated method stub
		HashMap<String, String> hash = null;
		try {
			// SQLiteDatabase db;
			SQLiteDatabase db = this.getWritableDatabase(); // Read Data

			String strSQL = "SELECT * FROM " + TABLE_CHAPTER_READ + " WHERE "
					+ COL_URL + " = '" + url + "'";
			Cursor cursor = db.rawQuery(strSQL, null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					hash = new HashMap<String, String>();
					hash.put(COL_TITLE, cursor.getString(0));
					hash.put(COL_DESC, cursor.getString(1));
					hash.put(COL_URL, cursor.getString(2));
				}
			}
			cursor.close();
			if (db != null) {
				db.close();
			}
			return hash;

		} catch (Exception e) {
			Log.i("getChapterReadByUrl", e.getMessage());
			return null;
		}

	}

	public ArrayList<HashMap<String, String>> getAllBookmark() {
		// TODO Auto-generated method stub

		try {
			ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

			SQLiteDatabase db = this.getWritableDatabase(); // Read Data

			String strSQL = "SELECT * FROM " + TABLE_BOOKMARK;
			Cursor cursor = db.rawQuery(strSQL, null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						HashMap<String, String> hash = new HashMap<String, String>();
						hash.put(COL_TITLE, cursor.getString(0));
						hash.put(COL_DESC, cursor.getString(1));
						hash.put(COL_IMG, cursor.getString(2));
						hash.put(COL_URL, cursor.getString(3));
						list.add(hash);
					} while (cursor.moveToNext());
				}
			}
			cursor.close();
			if (db != null) {
				db.close();
			}
			return list;

		} catch (Exception e) {
			Log.i("getAllBookmark", e.getMessage());
			// System.out.println("ERROR getAllBookmark: "+e);
			return null;
		}

	}

	public ArrayList<HashMap<String, String>> getAllChapterRead() {
		// TODO Auto-generated method stub

		try {
			ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

			SQLiteDatabase db = this.getWritableDatabase(); // Read Data

			String strSQL = "SELECT * FROM " + TABLE_CHAPTER_READ;
			Cursor cursor = db.rawQuery(strSQL, null);

			if (cursor != null) {
				if (cursor.moveToFirst()) {
					do {
						HashMap<String, String> hash = new HashMap<String, String>();
						hash.put(COL_TITLE, cursor.getString(0));
						hash.put(COL_DESC, cursor.getString(1));
						hash.put(COL_URL, cursor.getString(2));
						list.add(hash);
					} while (cursor.moveToNext());
				}
			}
			cursor.close();
			if (db != null) {
				db.close();
			}
			return list;

		} catch (Exception e) {
			Log.i("getAllChapterRead", e.getMessage());
			// System.out.println("ERROR getAllBookmark: "+e);
			return null;
		}

	}

	// Delete Data
	public boolean deleteBookmarkByTitle(String title) {
		// TODO Auto-generated method stub

		try {

			SQLiteDatabase db;
			db = this.getWritableDatabase(); // Write Data

			/**
			 * for API 11 and above SQLiteStatement insertCmd; String strSQL =
			 * "DELETE FROM " + TABLE_MEMBER + " WHERE MemberID = ? ";
			 * 
			 * insertCmd = db.compileStatement(strSQL); insertCmd.bindString(1,
			 * strMemberID);
			 * 
			 * return insertCmd.executeUpdateDelete();
			 * 
			 */

			db.delete(TABLE_BOOKMARK, COL_TITLE + " = ?",
					new String[] { String.valueOf(title) });

			db.close();
			return true; // return rows delete.

		} catch (Exception e) {
			return false;
		}

	}
	
	// Delete Data
		public boolean deleteChapterByUrl(String url) {
			// TODO Auto-generated method stub

			try {

				SQLiteDatabase db;
				db = this.getWritableDatabase(); // Write Data

				/**
				 * for API 11 and above SQLiteStatement insertCmd; String strSQL =
				 * "DELETE FROM " + TABLE_MEMBER + " WHERE MemberID = ? ";
				 * 
				 * insertCmd = db.compileStatement(strSQL); insertCmd.bindString(1,
				 * strMemberID);
				 * 
				 * return insertCmd.executeUpdateDelete();
				 * 
				 */

				db.delete(TABLE_CHAPTER_READ, COL_URL + " = ?",
						new String[] { String.valueOf(url) });

				db.close();
				return true; // return rows delete.

			} catch (Exception e) {
				return false;
			}

		}
}
