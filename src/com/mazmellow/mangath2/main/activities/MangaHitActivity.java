/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.mazmellow.mangath2.main.activities;

import static com.mazmellow.mangath2.uil.Constants.IMAGES;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import buzzcity.android.sdk.BCAdsClientBanner;

import com.mazmellow.mangath2.R;
import com.mazmellow.mangath2.main.DatabaseManager;
import com.mazmellow.mangath2.main.MangaHttpParse;
import com.mazmellow.mangath2.main.RequestHttpClient;
import com.mazmellow.mangath2.main.MangaHttpParse.MangaHttpParseListenner;
import com.mazmellow.mangath2.main.RequestHttpClient.RequestHttpClientListenner;
import com.mazmellow.mangath2.uil.AbsListViewBaseActivity;
import com.mazmellow.mangath2.uil.Constants.Extra;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.searchboxsdk.android.StartAppSearch;

/**
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 */
public class MangaHitActivity extends AbsListViewBaseActivity implements MangaHttpParseListenner{

	DisplayImageOptions options;
	public static ArrayList<HashMap<String, String>> hitList;

	String[] imageUrls;
	String[] titles;
	String[] urls;
	String[] descs;
	String[] tags;
	
//	boolean openChapter;
	MangaHttpParse httpParse;
	DatabaseManager myDb;
	ItemAdapter adapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ac_image_list);
		StartAppSearch.showSearchBox(this);
		
		imageUrls = new String[hitList.size()];
		titles = new String[hitList.size()];
		descs = new String[hitList.size()];
		urls = new String[hitList.size()];
		tags = new String[hitList.size()];
		
		for(int i=0; i<hitList.size(); i++){
			HashMap<String, String> hash = hitList.get(i);
			titles[i] = hash.get(MangaHttpParse.MANGA_TITLE);
			descs[i] = hash.get(MangaHttpParse.MANGA_DESC);
			imageUrls[i] = hash.get(MangaHttpParse.MANGA_IMG);
			urls[i] = hash.get(MangaHttpParse.MANGA_URL);
			tags[i] = hash.get(MangaHttpParse.MANGA_TAG);
			
			Log.i("title", MangaHttpParse.MANGA_TITLE);
			Log.i("descs", MangaHttpParse.MANGA_DESC);
			Log.i("imageUrls", MangaHttpParse.MANGA_IMG);
			Log.i("urls", MangaHttpParse.MANGA_URL);
			Log.i("tags", MangaHttpParse.MANGA_TAG);
			System.out.println("----------------------------");
		}

		options = new DisplayImageOptions.Builder()
			.showStubImage(R.drawable.ic_stub)
			.showImageForEmptyUri(R.drawable.ic_empty)
			.showImageOnFail(R.drawable.ic_error)
			.cacheInMemory(true)
			.cacheOnDisc(true)
			.displayer(new RoundedBitmapDisplayer(20))
			.build();

		listView = (ListView) findViewById(android.R.id.list);
		adapter = new ItemAdapter();
		((ListView) listView).setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ImagePagerActivity.mangaSel = titles[position]+descs[position];
				prepareCartoon(ImagePagerActivity.mangaSel, urls[position]);
				HomeActivity.readChapterManga(titles[position], descs[position], urls[position]);
				adapter.notifyDataSetChanged();
			}
		});
		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int position, long id) {
				// TODO Auto-generated method stub
				showOpenManga(titles[position], descs[position], urls[position], imageUrls[position]);
				return false;
			}
		});
		
		myDb = new DatabaseManager(this);
		HomeActivity.showTipBookmark(this);
		
		BCAdsClientBanner graphicAdClient = new BCAdsClientBanner(106400,
				BCAdsClientBanner.ADTYPE_MWEB,
				BCAdsClientBanner.IMGSIZE_MWEB_216x36, this);
		ImageView graphicalAds = (ImageView) findViewById(R.id.ads1);
		graphicAdClient.getGraphicalAd(graphicalAds);
	}
	
	protected void showOpenManga(final String title, final String desc, final String url, final String image) {
		// TODO Auto-generated method stub
		 
		// http://www.cartoonclub-th.com/one-piece/721/
		int lastIndex = url.indexOf("/", url.indexOf(".com")+5);
		final String newUrl = url.substring(0, lastIndex+1);
		
		// create a Dialog component
		final Dialog dialog = new Dialog(this);

		//tell the Dialog to use the dialog.xml as it's layout description
		dialog.setContentView(R.layout.dialog);
		dialog.setTitle(title);

//		TextView txt = (TextView) dialog.findViewById(R.id.tvTipName);
//		txt.setText("This is an Android custom Dialog Box Example! Enjoy!");

		Button gotoButton = (Button) dialog.findViewById(R.id.btnGoto);
		gotoButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImagePagerActivity.mangaSel = title + desc;
				openManga(image, newUrl);
				dialog.dismiss();
			}
		});
		
		Button bookmarkButton = (Button) dialog.findViewById(R.id.btnBookmark);
		bookmarkButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onBookmark(title, "", newUrl, image);
				dialog.dismiss();
			}
		});
		
		Button unReadButton = (Button) dialog.findViewById(R.id.btnUnRead);
		unReadButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				System.out.println("url: "+url);
				HomeActivity.unReadChapterManga(url);
				adapter.notifyDataSetChanged();
				dialog.dismiss();
			}
		});
		
		Button addTagButton = (Button) dialog.findViewById(R.id.btnAddTag);
		addTagButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				showDialogAddTag(title);
			}
		});
		
		Button cancelButton = (Button) dialog.findViewById(R.id.btnCancel);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();

	}
	
	protected void showDialogAddTag(final String title) {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(this);

		// tell the Dialog to use the dialog.xml as it's layout description
		dialog.setContentView(R.layout.dialog_addtag);
		dialog.setTitle("ร่วมเพิ่มคำค้นหาให้การ์ตูน " + title);
		// TextView addTagTitle =
		// (TextView)dialog.findViewById(R.id.addTagTitle);
		// addTagTitle.setText("ร่วมช่วยกันเพิ่มคำค้นหาให้กับการ์ตูน "+title+" กันนะคะ");

		final EditText editText2 = (EditText) dialog
				.findViewById(R.id.editText2);

		Button okButton = (Button) dialog.findViewById(R.id.btnAddTagOk);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String keyword = editText2.getEditableText().toString();
				keyword = keyword.trim();
				try {
					keyword = URLEncoder.encode(keyword, "utf-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("keyword = " + keyword);
				if (keyword != null && !keyword.equals("")) {
					requestAddTag(title, keyword);
					dialog.dismiss();
				}
			}
		});

		Button cancelButton = (Button) dialog
				.findViewById(R.id.btnAddTagCancel);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}
	
	protected void requestAddTag(String title, String keyword) {
		// TODO Auto-generated method stub
		final String backUpTitle = title;
		try {
			title = URLEncoder.encode(title, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String url = "http://www.mazmellow.com/readmangath/addTagManga.php?title=" + title
				+ "&tag=" + keyword;
		RequestHttpClient httpClient = new RequestHttpClient(url,
				new RequestHttpClientListenner() {
					@Override
					public void onRequestStringCallback(String response) {
						// TODO Auto-generated method stub
						ArrayList<HashMap<String, String>> updateList = new ArrayList<HashMap<String, String>>();

						try {
							JSONArray jsonArray = new JSONArray(response);

							if (jsonArray != null && jsonArray.length() > 0) {

								for (int i = 0; i < jsonArray.length(); i++) {
									HashMap<String, String> updateMangaHash = new HashMap<String, String>();

									JSONObject jsonObj = jsonArray
											.getJSONObject(i);
									// if (jsonObj.has("mid"))
									// updateMangaHash.put(MANGA_MID,
									// jsonObj.getString("mid"));
									if (jsonObj.has("title"))
										updateMangaHash.put(MangaHttpParse.MANGA_TITLE,
												jsonObj.getString("title"));
									if (jsonObj.has("tag"))
										updateMangaHash.put(MangaHttpParse.MANGA_TAG,
												jsonObj.getString("tag"));
									if (jsonObj.has("img"))
										updateMangaHash.put(MangaHttpParse.MANGA_IMG,
												jsonObj.getString("img"));
									if (jsonObj.has("url"))
										updateMangaHash.put(MangaHttpParse.MANGA_URL,
												jsonObj.getString("url"));

									updateMangaHash.put(MangaHttpParse.MANGA_DESC, "");
									updateList.add(updateMangaHash);

								}
							}

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.out.println("error = " + e.getMessage());
						}

						MangaListActivity.mangaList = updateList;
						Toast.makeText(MangaHitActivity.this,
								"ขอบคุณสำหรับการเพิ่มคำค้นหาการ์ตูน " + backUpTitle + " มากนะคะ",
								Toast.LENGTH_LONG).show();
					}
				}, this);
		httpClient.start();
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		adapter.notifyDataSetChanged();
	}

	public void onBookmark(String title, String desc, String url, String img) {
		//save by title & url
		String head = img.substring(0, img.lastIndexOf("_")+1);
		head += "200x0";
		head += img.substring(img.lastIndexOf("."));
		img = head;
		
//		if(!url.endsWith("?all")){
//			url += "?all";
//		}
			
		Log.i("title", title);
		Log.i("desc", desc);
		Log.i("img", img);
		Log.i("url", url);
		
		if(myDb.insertOrUpdateBookmark(title, desc, img, url)){
			Toast.makeText(MangaHitActivity.this, "เพิ่มการ์ตูน "+title+" ไปยังรายการโปรดสำเร็จแล้วค่ะ",
					Toast.LENGTH_LONG).show();
		}else{
			Toast.makeText(MangaHitActivity.this, "ขออภัยค่ะ เพิ่มการ์ตูน "+title+" ไปยังรายการโปรดไม่สำเร็จ โปรดลองอีกครั้งค่ะ",
					Toast.LENGTH_LONG).show();
		}
	}
	
	protected void openManga(String img, String url) {
		// TODO Auto-generated method stub
//		openChapter = true;
		MangaChapterActivity.url_manga = url;
		MangaChapterActivity.page = 1;
		ArrayList<HashMap<String, String>> list = HomeActivity.listUpdatePage
				.get("CHAP" + MangaListActivity.getKeyFromUrl(MangaChapterActivity.url_manga)+1);
		if (list != null && list.size() > 0) {
			listHitUpdate(null, list);
		} else {
			httpParse = new MangaHttpParse(url,
					MangaHttpParse.PARSE_LIST_CHAPTER, this, this);
			httpParse.start();
		}
	}
	
	private void prepareCartoon(String key, String url) {
		ArrayList<String> listImg = HomeActivity.allImage.get(key);
		if (listImg == null || listImg.size() == 0) {
			// must start tread to request html then parsing
			Log.i("url", url);
			httpParse = new MangaHttpParse(url,
					MangaHttpParse./*PARSE_ALL*/PARSE_ALL_UPDATE, this, this);
			httpParse.start();
		} else {
			prepareImages(listImg);
		}
	}

	@Override
	public void onBackPressed() {
		AnimateFirstDisplayListener.displayedImages.clear();
		super.onBackPressed();
	}

	class ItemAdapter extends BaseAdapter {

		private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

		private class ViewHolder {
			public TextView text;
			public TextView desc;
			public ImageView image;
			public TextView read;
		}

		@Override
		public int getCount() {
			return imageUrls.length;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;
			if (convertView == null) {
				view = getLayoutInflater().inflate(R.layout.item_list_image, parent, false);
				holder = new ViewHolder();
				holder.text = (TextView) view.findViewById(R.id.text);
				holder.desc = (TextView) view.findViewById(R.id.desc);
				holder.image = (ImageView) view.findViewById(R.id.image);
				holder.read = (TextView) view.findViewById(R.id.read);
				
				
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			holder.text.setText(titles[position]);
			holder.desc.setText(descs[position]);
			imageLoader.displayImage(imageUrls[position], holder.image, options, animateFirstListener);
			
			System.out.println("url = "+urls[position]);
			System.out.println("tag = "+tags[position]);
			if(HomeActivity.isChapterReaded(urls[position])){
				holder.read.setText("R");
				holder.read.setTextColor(0xFF99CC00);
			}else{
				if(tags[position]!=null && !tags[position].equalsIgnoreCase("") ){
					holder.read.setText("N");
					holder.read.setTextColor(0xFFFF4444);
				}else{
					holder.read.setText("");
				}
				
			}

			return view;
		}
	}

	private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

	@Override
	public void prepareImages(ArrayList<String> listImg) {
		IMAGES = new String[listImg.size()];
		for (int i = 0; i < listImg.size(); i++) {
			IMAGES[i] = (String) listImg.get(i);
		}
		Intent intent = new Intent(this, ImagePagerActivity.class);
		intent.putExtra(Extra.IMAGES, IMAGES);
		startActivity(intent);
	}

	@Override
	public void listHitUpdate(ArrayList<HashMap<String, String>> hitList,
			ArrayList<HashMap<String, String>> updateList) {
		// TODO Auto-generated method stub
//		if(!openChapter){
//			MangaUpdateActivity.updateList = updateList;
//			HomeActivity.listUpdatePage.put("LIST" + page, updateList);
//
//			imageUrls = new String[updateList.size()];
//			titles = new String[updateList.size()];
//			descs = new String[updateList.size()];
//			urls = new String[updateList.size()];
//			for (int i = 0; i < updateList.size(); i++) {
//				HashMap<String, String> hash = updateList.get(i);
//				titles[i] = (String) hash.get(MangaHttpParse.MANGA_TITLE);
//				descs[i] = (String) hash.get(MangaHttpParse.MANGA_DESC);
//				imageUrls[i] = (String) hash.get(MangaHttpParse.MANGA_IMG);
//				urls[i] = (String) hash.get(MangaHttpParse.MANGA_URL);
//
//				Log.i("title", titles[i]);
//				Log.i("descs", descs[i]);
//				Log.i("imageUrls", imageUrls[i]);
//				Log.i("urls", urls[i]);
//				System.out.println("----------------------------");
//			}
//
//			adapter.notifyDataSetChanged();
//			pageTextView.setText("หน้าที่ " + page);
//		}else{
			MangaChapterActivity.chapterList = updateList;
			HomeActivity.listUpdatePage.put("CHAP" + MangaListActivity.getKeyFromUrl(MangaChapterActivity.url_manga)+1, updateList);
			
			Intent intent = new Intent(this, MangaChapterActivity.class);
			intent.putExtra(MangaChapterActivity.URL, MangaChapterActivity.url_manga);
			startActivity(intent);
//		}
	}
}