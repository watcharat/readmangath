/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.mazmellow.mangath2.main.activities;

import it.sephiroth.android.library.imagezoom.ImageViewTouch.OnPageScaleListener;

import java.util.Random;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.appflood.AppFlood;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.InterstitialAd;
import com.matabii.dev.scaleimageview.ScaleImageView;
import com.mazmellow.mangath2.R;
import com.mazmellow.mangath2.main.AdsWrapper;
import com.mazmellow.mangath2.main.DeactivableViewPager;
import com.mazmellow.mangath2.main.DeactivableViewPager.DeactivableViewPagerListener;
import com.mazmellow.mangath2.uil.BaseActivity;
import com.mazmellow.mangath2.uil.Constants.Extra;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

/**
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 */
public class ImagePagerActivity extends BaseActivity implements /*AdListener,*/
		DeactivableViewPagerListener, OnSeekBarChangeListener {

	private static final String STATE_POSITION = "STATE_POSITION";

	DisplayImageOptions options;

	DeactivableViewPager pager;

	//private InterstitialAd interstitial;

	public static String mangaSel;

	static boolean pageChange;

	boolean isShow;
	SeekBar seekbar;
	TextView textSeek;
	int pageNum;
	int pageNumMax;

	int countPageChanged;

	// Timer timer;
	// MyTask timerTask;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.ac_image_pager);

		Bundle bundle = getIntent().getExtras();
		String[] imageUrls = bundle.getStringArray(Extra.IMAGES);
		int pagerPosition = bundle.getInt(Extra.IMAGE_POSITION, 0);

		if (savedInstanceState != null) {
			pagerPosition = savedInstanceState.getInt(STATE_POSITION);
		}

		// clear cache image
		// imageLoader.clearMemoryCache();
		// imageLoader.clearDiscCache();

		pageChange = false;

		options = new DisplayImageOptions.Builder()
				.showImageForEmptyUri(R.drawable.ic_empty)
				.showImageOnFail(R.drawable.ic_error)
				.resetViewBeforeLoading(true).cacheOnDisc(true)
				.imageScaleType(ImageScaleType.EXACTLY)
				.bitmapConfig(Bitmap.Config.RGB_565)
				.displayer(new FadeInBitmapDisplayer(300)).build();

		pager = (DeactivableViewPager) findViewById(R.id.pager);
		pager.setAdapter(new ImagePagerAdapter(imageUrls));
		pager.setCurrentItem(pagerPosition);

		pager.setOnPageChangeListener(new OnPageChangeListener() {

			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub
				pageChange = true;
				pager.activate();
			}

			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
			}

			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});

		// ImageLoaderConfiguration config = new
		// ImageLoaderConfiguration.Builder(
		// getApplicationContext()).memoryCacheExtraOptions(3000, 3000)
		// .build();

		showTipPinch();

		// BCAdsClientBanner graphicAdClient = new BCAdsClientBanner(101021,
		// BCAdsClientBanner.ADTYPE_MWEB,
		// BCAdsClientBanner.IMGSIZE_MWEB_216x36, this);
		// ImageView graphicalAds = (ImageView) findViewById(R.id.ads6);
		// graphicAdClient.getGraphicalAd(graphicalAds);

		pager.setDeactivableViewPagerListenner(this);
		isShow = false;
		seekbar = (SeekBar) findViewById(R.id.seekbar);
		pageNum = 1;
		pageNumMax = imageUrls.length;
		seekbar.setProgress(pageNum);
		seekbar.setMax(pageNumMax);
		seekbar.setOnSeekBarChangeListener(this);
		countPageChanged = 0;

		textSeek = (TextView) findViewById(R.id.textSeek);
		textSeek.setText(pageNum + "/" + pageNumMax);

		// timer = new Timer();
		// timerTask = new MyTask();
		// timer.schedule(timerTask, 0, 1000);
	}

	protected void onDestroy() {
		// TODO Auto-generated method stub

		System.out.println("onDestroy() countPageChanged: " + countPageChanged);

		if (countPageChanged >= 7) {

			Random r = new Random();
			int index = r.nextInt(8);
			switch (index) {
			case 0:
				AppFlood.showPanel(this, AppFlood.PANEL_TOP);
				break;
			case 1:
				AppFlood.showPanel(this, AppFlood.PANEL_BOTTOM);
				break;
			case 2:
				AppFlood.showList(this, AppFlood.LIST_TAB_APP);
				break;
			case 3:
				AppFlood.showList(this, AppFlood.LIST_ALL);
				break;
			case 4: {

				// Buzzcity
				 Intent i = new Intent(getApplicationContext(),
				 AdsWrapper.class);
				 i.putExtra("partnerId", "106400");
				 i.putExtra("appId", "com.mazmellow.mangath2");
				 i.putExtra("showAt", "start");
				 i.putExtra("skipEarly", "true");
				 i.putExtra("adsTimeout", "10");
				 startActivity(i);

				// Create the interstitial
//				interstitial = new InterstitialAd(this, "a152be5d2d2c180"
//				/* "a152be6d5ad8b67 591e87c681fd4760" */);
//
//				// Create ad request
//				AdRequest adRequest = new AdRequest();
//
//				// Begin loading your interstitial
//				interstitial.loadAd(adRequest);
//
//				// Set Ad Listener to use the callbacks below
//				interstitial.setAdListener(this);

			}
			case 5:
				AppFlood.showList(this, AppFlood.LIST_TAB_GAME);
				break;
			case 6:
				AppFlood.showFullScreen(this);
				break;
			case 7:
				AppFlood.showInterstitial(this);
				break;

			default:
				break;
			}

		}

		super.onDestroy();
	}

	// class MyTask extends TimerTask {
	// int currentNumber = 0;
	//
	//
	// public void run() {
	// if (currentNumber <= 3) {
	// //set seekbar hidden
	// isShow = false;
	// seekbar.setVisibility(View.GONE);
	// timer.cancel();
	// }
	// }
	// }

	static SharedPreferences sharedPref;
	public static final String SHOW_TIP_MANGA_PINCH = "show_tip_manga_pinch";

	public void showTipPinch() {
		if (sharedPref == null) {
			sharedPref = getSharedPreferences(
					getString(R.string.preference_file_key),
					Context.MODE_PRIVATE);
		}
		boolean isShow = sharedPref.getBoolean(SHOW_TIP_MANGA_PINCH, false);
		Log.i("isShow", isShow + "");
		if (!isShow) {
			// not save >> show
			View checkBoxView = View.inflate(this, R.layout.checkbox, null);
			CheckBox checkBox = (CheckBox) checkBoxView
					.findViewById(R.id.checkbox);
			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {

					// Save to shared preferences
					SharedPreferences.Editor editor = sharedPref.edit();
					editor.putBoolean(SHOW_TIP_MANGA_PINCH, true);
					editor.commit();
				}
			});
			checkBox.setText("ไม่แสดงข้อความนี้อีก");

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Tips");
			builder.setMessage(
					"คุณสามารถซูมเข้า-ออกได้ โดยการ \"double click\" (ใช้นิ้วแตะ 2 ที) หรือ \"pinch to zoom\" (ใช้นิ้ว 2 นิ้วถ่างออกจากกัน) \n หากซูมเข้าแล้วต้องการเปลี่ยนหน้า ให้ซูมออกก่อนนะคะ")
					.setView(checkBoxView)
					.setCancelable(false)
					.setPositiveButton("ตกลง",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							}).show();
		}
	}

	// public void onReceiveAd(Ad ad) {
	// System.out.println("onReceiveAd");
	// // if (ad == interstitial) {
	// // interstitial.show();
	// // }
	// }

	public void onSaveInstanceState(Bundle outState) {
		outState.putInt(STATE_POSITION, pager.getCurrentItem());
	}

	private class ImagePagerAdapter extends PagerAdapter {

		private String[] images;
		private LayoutInflater inflater;

		// PhotoViewAttacher mAttacher;

		ImagePagerAdapter(String[] images) {
			this.images = images;
			inflater = getLayoutInflater();
		}

		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((View) object);
		}

		public void finishUpdate(View container) {
		}

		public int getCount() {
			return images.length;
		}

		public Object instantiateItem(ViewGroup view, int position) {
			View imageLayout = inflater.inflate(R.layout.item_pager_image,
					view, false);
			// final ImageViewTouch imageView = (ImageViewTouch) imageLayout
			// .findViewById(R.id.image);

			ScaleImageView imageView = (ScaleImageView) imageLayout
					.findViewById(R.id.image);

			// PhotoView imageView = (PhotoView) imageLayout
			// .findViewById(R.id.image);

			final ProgressBar spinner = (ProgressBar) imageLayout
					.findViewById(R.id.loading);

			imageView.setOnScaleListener(new OnPageScaleListener() {

				public void onScaleBegin() {
					pager.deactivate();
				}

				public void onScaleEnd(float scale) {

					if (pageChange) {
						return;
					}

					System.out.println("scale = " + scale);
					if (scale > 1.0) {
						pager.deactivate();
					} else {
						pager.activate();
					}
				}

				public void pagerEnable(boolean enable) {
					// TODO Auto-generated method stub
					if (enable) {
						pager.activate();
					} else {
						pager.deactivate();
					}
				}

				public void onZooming(boolean zooming) {
					// TODO Auto-generated method stub
					if (zooming) {
						pageChange = false;
					} else {
						pageChange = true;
					}
				}

				public void onTouch() {
					// TODO Auto-generated method stub
					if (!isShow) {
						isShow = true;
						seekbar.setVisibility(View.VISIBLE);
						seekbar.setProgress(pageNum);

						textSeek.setText(pageNum + "/" + pageNumMax);
						textSeek.setVisibility(View.VISIBLE);
						// timer.schedule(timerTask, 0, 1000);
					} else {
						isShow = false;
						seekbar.setVisibility(View.GONE);
						textSeek.setVisibility(View.GONE);

						// timer.cancel();
					}
				}
			});
			// imageView.forceZoom(1.0f);

			imageLoader.displayImage(images[position], imageView, options,
					new SimpleImageLoadingListener() {

						public void onLoadingStarted(String imageUri, View view) {
							spinner.setVisibility(View.VISIBLE);
						}

						public void onLoadingFailed(String imageUri, View view,
								FailReason failReason) {
							spinner.setVisibility(View.GONE);
						}

						public void onLoadingComplete(String imageUri,
								View view, Bitmap loadedImage) {
							spinner.setVisibility(View.GONE);
							// imageView.forceZoom(1.0f);
						}
					});

			((ViewPager) view).addView(imageLayout, LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT);
			// view.addView(imageView, LayoutParams.MATCH_PARENT,
			// LayoutParams.MATCH_PARENT);

			// mAttacher.update();
			return imageLayout;
		}

		public boolean isViewFromObject(View view, Object object) {
			return view.equals(object);
		}

		public void restoreState(Parcelable state, ClassLoader loader) {
		}

		public Parcelable saveState() {
			return null;
		}

		public void startUpdate(View container) {
		}
	}

	public void onDismissScreen(Ad arg0) {
		// TODO Auto-generated method stub
		System.out.println("onDismissScreen");
	}

	public void onFailedToReceiveAd(Ad arg0, ErrorCode arg1) {
		// TODO Auto-generated method stub
		System.out.println("onFailedToReceiveAd");
	}

	public void onLeaveApplication(Ad arg0) {
		// TODO Auto-generated method stub
		System.out.println("onLeaveApplication");
	}

	public void onPresentScreen(Ad arg0) {
		// TODO Auto-generated method stub
		System.out.println("onPresentScreen");
	}

	public void onPageChange(int pageNumber) {
		// TODO Auto-generated method stub
		System.out.println("ImagePager onPageChange: " + pageNumber);
		// set seek bar
		pageNum = pageNumber + 1;
		seekbar.setProgress(pageNum);
		textSeek.setText(pageNum + "/" + pageNumMax);

		countPageChanged++;
	}

	// SEEK ------------

	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub
		// System.out.println("onProgressChanged() progress: "+progress+
		// ", fromUser: "+fromUser);

		// force viewPager goto page
		if (fromUser) {
			pager.setCurrentItem(progress);
			pageNum = progress;
			textSeek.setText(pageNum + "/" + pageNumMax);
		}
	}

	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		System.out.println("-- onStartTrackingTouch --");
	}

	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		System.out.println("-- onStopTrackingTouch --");

		// timer.schedule(timerTask, 0, 1000);
	}

//	@Override
//	public void onReceiveAd(Ad arg0) {
//		// TODO Auto-generated method stub
//		if (interstitial.isReady()) {
//			interstitial.show();
//		}
//	}

}