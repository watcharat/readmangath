/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.mazmellow.mangath2.main.activities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import buzzcity.android.sdk.BCAdsClientBanner;

import com.appflood.AppFlood;
import com.mazmellow.mangath2.R;
import com.mazmellow.mangath2.main.DatabaseManager;
import com.mazmellow.mangath2.main.MangaHttpParse;
import com.mazmellow.mangath2.main.MangaHttpParse.MangaHttpParseListenner;
import com.mazmellow.mangath2.main.RequestHttpClient;
import com.mazmellow.mangath2.main.RequestHttpClient.RequestHttpClientListenner;
import com.mazmellow.mangath2.uil.BaseActivity;
import com.nostra13.universalimageloader.utils.L;
import com.searchboxsdk.android.StartAppSearch;
import com.startapp.android.publish.StartAppAd;

/**
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 */
public class HomeActivity extends BaseActivity implements
		MangaHttpParseListenner {

	public static final String STARTAPP_DEVID = "101337423";
	public static final String STARTAPP_ID = "201835855";

	public static final String APPFLOOD_APPKEY = "AmtYH7VPenuUmRc6";
	public static final String APPFLOOD_SECRET = "8CQLEBsv3004L52e4b30d";

	public static final String URL_MANGA = "http://www.cartoonclub-th.com/";// "http://www.cartoonclub-th.com/";
	public static final String URL_LIST_MANGA = "http://mazmellow.com/readmangath/getListManga.php";// "http://www.cartoonclub-th.com/manga-list/all/any/name-az/1/";//
																									// "http://www.cartoonclub-th.com/manga-list/all/any/name-az/1";
	public static Hashtable<String, ArrayList<String>> allImage;
	public static Hashtable<String, ArrayList<HashMap<String, String>>> listUpdatePage;

	private static final String TEST_FILE_NAME = "Universal Image Loader @#&=+-_.,!()~'%20.png";

	MangaHttpParse httpParse;
	boolean requestMangaSuccess;
	boolean requestListMangaSuccess;
	boolean openHit;
	boolean openUpdate;

	boolean isClickList;
	boolean openList;

	public static ArrayList<String> chapterReadList;
	public static DatabaseManager dbMgr;

	private StartAppAd startAppAd = new StartAppAd(this);

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		StartAppAd.init(this, STARTAPP_DEVID, STARTAPP_ID);
		StartAppSearch.init(this, STARTAPP_DEVID, STARTAPP_ID);

		AppFlood.initialize(this, APPFLOOD_APPKEY, APPFLOOD_SECRET,
				AppFlood.AD_ALL);

		setContentView(R.layout.ac_home);

		StartAppSearch.showSearchBox(this);

		// startAppAd.showAd(); // show the ad
		startAppAd.loadAd(); // load the next ad

		// AppFlood.showFullScreen(this);
		// AppFlood.showPanel(this, AppFlood.PANEL_TOP);
		// AppFlood.showList(this,AppFlood.LIST_TAB_APP);
		// AppFlood.showInterstitial(this);
		// AppFlood.showBanner(this,AppFlood.BANNER_POSITION_BOTTOM,AppFlood.BANNER_SMALL
		// );

		allImage = new Hashtable<String, ArrayList<String>>();
		listUpdatePage = new Hashtable<String, ArrayList<HashMap<String, String>>>();

		// File testImageOnSdCard = new File("/mnt/sdcard", TEST_FILE_NAME);
		// if (!testImageOnSdCard.exists()) {
		// copyTestImageToSdCard(testImageOnSdCard);
		// }

		// FrameLayout layout = (FrameLayout) findViewById(R.id.homeLayout);
		// BCADAdView adView = new BCADAdView(
		// this.getApplicationContext(),
		// BCADAdType.BCAD_TYPE_IMAGE,
		// BCADAdSize.BCAD_SIZE_320x50);
		// adView.partnerID = "101021";
		// adView.setApplicationID("d85cb745038c2eca2f0b2611aa3ea1c5");
		// adView.setLayoutParams(new
		// FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
		// LayoutParams.WRAP_CONTENT,
		// Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL));
		// BCADRequest request = new BCADRequest();
		// request.autorefresh = 60;
		//
		// layout.addView(adView);
		// requestManga();

		BCAdsClientBanner graphicAdClient = new BCAdsClientBanner(101021,
				BCAdsClientBanner.ADTYPE_MWEB,
				BCAdsClientBanner.IMGSIZE_MWEB_216x36, this);
		ImageView graphicalAds = (ImageView) findViewById(R.id.ads);
		graphicAdClient.getGraphicalAd(graphicalAds);

		dbMgr = new DatabaseManager(this);
		chapterReadList = new ArrayList<String>();
		ArrayList<HashMap<String, String>> listChapterReadHash = dbMgr
				.getAllChapterRead();
		for (int i = 0; i < listChapterReadHash.size(); i++) {
			HashMap<String, String> hash = listChapterReadHash.get(i);
			String url = hash.get("url");
			if (!url.endsWith("/")) {
				url += "/";
			}
			System.out.println("-- url chapter read: " + url);
			chapterReadList.add(url);
		}

	}

	@Override
	public void onPause() {
		super.onPause();
		startAppAd.onPause();
	}

	@Override
	public void onBackPressed() {
		imageLoader.stop();
		startAppAd.onBackPressed();
		super.onBackPressed();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		startAppAd.onResume();
	}

	public static void readChapterManga(String title, String desc, String url) {
		// insert readed chapter into

		if (!url.endsWith("/")) {
			url += "/";
		}

		if (chapterReadList.indexOf(url) == -1) {
			// insert
			chapterReadList.add(url);
			dbMgr.insertOrUpdateChapterRead(title, desc, url);
		}
	}

	public static void unReadChapterManga(String url) {
		// insert readed chapter into

		if (!url.endsWith("/")) {
			url += "/";
		}

		if (chapterReadList.indexOf(url) != -1) {
			// insert
			chapterReadList.remove(url);
			dbMgr.deleteChapterByUrl(url);
		}
	}

	public static boolean isChapterReaded(String url) {
		if (!url.endsWith("/")) {
			url += "/";
		}
		boolean isRead = false;
		if (chapterReadList.indexOf(url) != -1) {
			isRead = true;
		}
		return isRead;
	}

	public void requestManga(boolean isList) {
		if (!isList) {
			httpParse = new MangaHttpParse(URL_MANGA,
					MangaHttpParse.PARSE_FIRST, this, this);
		} else {
			httpParse = new MangaHttpParse(URL_LIST_MANGA,
					MangaHttpParse.PARSE_LIST_MANGA2, this, this);
		}

		httpParse.start();
	}

	public void onClickReadHit(View view) {

		if (MangaHitActivity.hitList != null
				&& MangaHitActivity.hitList.size() > 0) {
			Intent intent = new Intent(this, MangaHitActivity.class);
			startActivity(intent);
		} else {
			String url = "http://mazmellow.com/readmangath/getHitManga.php";
			RequestHttpClient httpClient = new RequestHttpClient(url,
					new RequestHttpClientListenner() {

						@Override
						public void onRequestStringCallback(String response) {
							// TODO Auto-generated method stub
							ArrayList<HashMap<String, String>> updateList = new ArrayList<HashMap<String, String>>();

							try {
								JSONArray jsonArray = new JSONArray(response);

								if (jsonArray != null && jsonArray.length() > 0) {

									for (int i = 0; i < jsonArray.length(); i++) {
										HashMap<String, String> updateMangaHash = new HashMap<String, String>();

										JSONObject jsonObj = jsonArray
												.getJSONObject(i);
										if (jsonObj.has("title"))
											updateMangaHash.put(MangaHttpParse.MANGA_TITLE,
													jsonObj.getString("title"));
										if (jsonObj.has("description"))
											updateMangaHash.put(MangaHttpParse.MANGA_DESC,
													jsonObj.getString("description"));
										if (jsonObj.has("img"))
											updateMangaHash.put(MangaHttpParse.MANGA_IMG,
													jsonObj.getString("img"));
										if (jsonObj.has("url"))
											updateMangaHash.put(MangaHttpParse.MANGA_URL,
													jsonObj.getString("url"));
										if (jsonObj.has("tag"))
											updateMangaHash.put(MangaHttpParse.MANGA_TAG,
													jsonObj.getString("tag"));
										updateList.add(updateMangaHash);

									}
								}

							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								System.out.println("error = " + e.getMessage());
							}
							
							MangaHitActivity.hitList = updateList;
							Intent intent = new Intent(HomeActivity.this, MangaHitActivity.class);
							startActivity(intent);
						}
					}, this);
			httpClient.start();
		}

		// isClickList = false;
		// if (requestMangaSuccess) {
		// Intent intent = new Intent(this, MangaHitActivity.class);
		// startActivity(intent);
		// } else {
		// requestManga(false);
		// openHit = true;
		// openUpdate = false;
		// }
	}

	public void onClickReadUpdate(View view) {
		isClickList = false;
		MangaUpdateActivity.page = 1;
		if (requestMangaSuccess) {
			Intent intent = new Intent(this, MangaUpdateActivity.class);
			startActivity(intent);
		} else {
			requestManga(false);
			openHit = false;
			openUpdate = true;
		}

	}

	public void onClickReadList(View view) {
		isClickList = true;
		MangaListActivity.page = 1;
		if (requestListMangaSuccess) {
			Intent intent = new Intent(this, MangaListActivity.class);
			startActivity(intent);
		} else {
			requestManga(true);
			openList = true;
		}
	}

	public void onClickRateUs(View view) {
		final String appName = "com.mazmellow.mangath2";
		try {
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("market://details?id=" + appName)));
		} catch (android.content.ActivityNotFoundException anfe) {
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://play.google.com/store/apps/details?id="
							+ appName)));
		}
	}

	public void onClickAnimeTH(View view) {

		Intent intent = new Intent(this, AppListActivity.class);
		startActivity(intent);

		// final String appName = "com.devchi.animeth";
		// try {
		// startActivity(new Intent(Intent.ACTION_VIEW,
		// Uri.parse("market://details?id=" + appName)));
		// } catch (android.content.ActivityNotFoundException anfe) {
		// startActivity(new Intent(Intent.ACTION_VIEW,
		// Uri.parse("http://play.google.com/store/apps/details?id="
		// + appName)));
		// }
	}

	private void copyTestImageToSdCard(final File testImageOnSdCard) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					InputStream is = getAssets().open(TEST_FILE_NAME);
					FileOutputStream fos = new FileOutputStream(
							testImageOnSdCard);
					byte[] buffer = new byte[8192];
					int read;
					try {
						while ((read = is.read(buffer)) != -1) {
							fos.write(buffer, 0, read);
						}
					} finally {
						fos.flush();
						fos.close();
						is.close();
					}
				} catch (IOException e) {
					L.w("Can't copy test image onto SD card");
				}
			}
		}).start();
	}

	@Override
	public void prepareImages(ArrayList<String> listImg) {

	}

	@Override
	public void listHitUpdate(ArrayList<HashMap<String, String>> hitList,
			ArrayList<HashMap<String, String>> updateList) {
		// TODO Auto-generated method stub
		if (!isClickList) {

			requestMangaSuccess = true;
			// MangaHitActivity.hitList = hitList;
			MangaUpdateActivity.updateList = updateList;
			listUpdatePage.put("PAGE" + 1, updateList);

			// if (openHit) {
			// onClickReadHit(null);
			// } else if (openUpdate) {
			onClickReadUpdate(null);
			// }
		} else {

			requestListMangaSuccess = true;
			MangaListActivity.mangaList = updateList;
			listUpdatePage.put("LIST" + 1, updateList);

			if (openList) {
				onClickReadList(null);
			}
		}
	}

	public void onClickBookmark(View view) {
		Intent intent = new Intent(this, BookmarkListActivity.class);
		startActivity(intent);
	}

	static SharedPreferences sharedPref;
	public static final String SHOW_TIP_MANGA = "show_tip_manga";

	public static void showTipBookmark(Context context) {
		if (sharedPref == null) {
			sharedPref = context.getSharedPreferences(
					context.getString(R.string.preference_file_key),
					Context.MODE_PRIVATE);
		}
		boolean isShow = sharedPref.getBoolean(SHOW_TIP_MANGA, false);
		Log.i("isShow", isShow + "");
		if (!isShow) {
			// not save >> show
			View checkBoxView = View.inflate(context, R.layout.checkbox, null);
			CheckBox checkBox = (CheckBox) checkBoxView
					.findViewById(R.id.checkbox);
			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {

					// Save to shared preferences
					SharedPreferences.Editor editor = sharedPref.edit();
					editor.putBoolean(SHOW_TIP_MANGA, true);
					editor.commit();
				}
			});
			checkBox.setText("ไม่แสดงข้อความนี้อีก");

			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle("Tips");
			builder.setMessage(
					"คุณสามารถกดค้างที่ลิสต์ของตอนการ์ตูน เพื่อไปยังหน้ารวมตอนของการ์ตูนเรื่องนั้นๆได้ หรือเพิ่มเป็นรายการโปรดก็ได้นะคะ")
					.setView(checkBoxView)
					.setCancelable(false)
					.setPositiveButton("ตกลง",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							}).show();
		}
	}

}