/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.mazmellow.mangath2.main.activities;

import static com.mazmellow.mangath2.uil.Constants.IMAGES;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import buzzcity.android.sdk.BCAdsClientBanner;

import com.mazmellow.mangath2.R;
import com.mazmellow.mangath2.main.DatabaseManager;
import com.mazmellow.mangath2.main.MangaHttpParse;
import com.mazmellow.mangath2.main.MangaHttpParse.MangaHttpParseListenner;
import com.mazmellow.mangath2.uil.AbsListViewBaseActivity;
import com.mazmellow.mangath2.uil.Constants.Extra;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.searchboxsdk.android.StartAppSearch;

/**
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 */
public class BookmarkListActivity extends AbsListViewBaseActivity implements
		MangaHttpParseListenner {

	public static final String SHOW_TUTORIAL_BOOKMARK = "show_tutorial_bookmark";
	
	DisplayImageOptions options;
	public static ArrayList<HashMap<String, String>> updateList;

	String[] imageUrls;
	String[] titles;
	String[] urls;
	String[] descs;

	public static int page = 1;

	MangaHttpParse httpParse;
	ItemAdapter adapter;

	// TextView pageTextView;
	DatabaseManager myDb;
	SharedPreferences sharedPref;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ac_image_list5);
		
		StartAppSearch.showSearchBox(this);

		options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_stub)
				.showImageForEmptyUri(R.drawable.ic_empty)
				.showImageOnFail(R.drawable.ic_error).cacheInMemory(true)
				.cacheOnDisc(true).displayer(new RoundedBitmapDisplayer(20))
				.build();

		listView = (ListView) findViewById(android.R.id.list);
		adapter = new ItemAdapter();
		((ListView) listView).setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				ImagePagerActivity.mangaSel = titles[position]
						+ descs[position];
				openManga(imageUrls[position], urls[position]);
			}
		});
		listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int position, long id) {
				// TODO Auto-generated method stub
				deleteBookmark(titles[position]);
				return false;
			}
		});

		myDb = new DatabaseManager(this);
		// myDb.getWritableDatabase();

		// pageTextView = (TextView) findViewById(R.id.textView4);
		// pageTextView.setText("‡∏´‡∏ô‡πâ‡∏≤‡∏ó‡∏µ‡πà " + page);

		refreshBookmark();
		
		BCAdsClientBanner graphicAdClient = new BCAdsClientBanner(101021,
				BCAdsClientBanner.ADTYPE_MWEB,
				BCAdsClientBanner.IMGSIZE_MWEB_216x36, this);
		ImageView graphicalAds = (ImageView) findViewById(R.id.ads5);
		graphicAdClient.getGraphicalAd(graphicalAds);
	}

	protected void deleteBookmark(final String title) {
		// TODO Auto-generated method stub
		new AlertDialog.Builder(this)
				.setTitle("ลบ " + title + " ?")
				.setMessage(
						"คุณแน่ใจแล้วหรือว่าต้องการลบการ์ตูน " + title
								+ " ออกจากรายการโปรด?")
				.setPositiveButton("ใช่",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// continue with delete
								myDb.deleteBookmarkByTitle(title);
								refreshBookmark();
							}
						})
				.setNegativeButton("ไม่",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// do nothing
							}
						}).show();

	}

	protected void openManga(String img, String url) {
		// TODO Auto-generated method stub
		MangaChapterActivity.url_manga = url;
		MangaChapterActivity.page = 1;
		ArrayList<HashMap<String, String>> list = HomeActivity.listUpdatePage
				.get("CHAP"
						+ MangaListActivity
								.getKeyFromUrl(MangaChapterActivity.url_manga)
						+ 1);
		if (list != null && list.size() > 0) {
			listHitUpdate(null, list);
		} else {
			httpParse = new MangaHttpParse(url,
					MangaHttpParse.PARSE_LIST_CHAPTER, this, this);
			httpParse.start();
		}
	}

	public void refreshBookmark() {
		// fetch database
		ArrayList<HashMap<String,String>> dbList = myDb.getAllBookmark();	
		updateList = new ArrayList<HashMap<String,String>>();
		if (dbList != null && dbList.size() > 0) {
			
			//filter url
			for (int i = 0; i < dbList.size(); i++) {
				HashMap<String, String> hash = dbList.get(i);
				String url = (String) hash.get(MangaHttpParse.MANGA_URL);
				if(MangaHttpParse.checkUrl(url)){
					updateList.add(hash);
				}
			}
			
			
			imageUrls = new String[updateList.size()];
			titles = new String[updateList.size()];
			descs = new String[updateList.size()];
			urls = new String[updateList.size()];
			for (int i = 0; i < updateList.size(); i++) {
				HashMap<String, String> hash = updateList.get(i);
				titles[i] = (String) hash.get(MangaHttpParse.MANGA_TITLE);
				descs[i] = (String) hash.get(MangaHttpParse.MANGA_DESC);
				imageUrls[i] = (String) hash.get(MangaHttpParse.MANGA_IMG);
				urls[i] = (String) hash.get(MangaHttpParse.MANGA_URL);

				Log.i("title", titles[i]);
				Log.i("descs", descs[i]);
				Log.i("imageUrls", imageUrls[i]);
				Log.i("urls", urls[i]);
				System.out.println("----------------------------");
			}

			adapter.notifyDataSetChanged();

			showCheckBox();

		} else {
			Toast.makeText(BookmarkListActivity.this,
					"คุณยังไม่มีรายการโปรดค่ะ", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	// private void refreshList() {
	// ArrayList<HashMap<String, String>> list = HomeActivity.listUpdatePage
	// .get("PAGE" + page);
	// if (list != null && list.size() > 0) {
	// listHitUpdate(null, list);
	// } else {
	// String url = "http://www.cartoonclub-th.com/latest-chapters/" + page;
	// httpParse = new MangaHttpParse(url,
	// MangaHttpParse.PARSE_UPDATE_LIST, this, this);
	// httpParse.start();
	// }
	//
	// }
	//
	// public void onClickPrevious(View view) {
	// if (page > 1) {
	// page--;
	// refreshList();
	// }
	// }
	//
	// public void onClickNext(View view) {
	// page++;
	// refreshList();
	// }

	private void showCheckBox() {
		if (sharedPref == null) {
			sharedPref = this.getSharedPreferences(
					getString(R.string.preference_file_key),
					Context.MODE_PRIVATE);
		}
		boolean isShow = sharedPref.getBoolean(SHOW_TUTORIAL_BOOKMARK, false);
		Log.i("isShow", isShow+"");
		if (!isShow) {
			// not save >> show
			View checkBoxView = View.inflate(this, R.layout.checkbox, null);
			CheckBox checkBox = (CheckBox) checkBoxView
					.findViewById(R.id.checkbox);
			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {

					// Save to shared preferences
					SharedPreferences.Editor editor = sharedPref.edit();
					editor.putBoolean(SHOW_TUTORIAL_BOOKMARK, true);
					editor.commit();
				}
			});
			checkBox.setText("ไม่แสดงข้อความนี้อีก");

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("ต้องการลบรายการโปรด?");
			builder.setMessage("หากคุณต้องการลบรายการโปรดของคุณ คุณสามารถกดค้างที่รายการเพื่อลบได้ค่ะ")
					.setView(checkBoxView)
					.setCancelable(false)
					.setPositiveButton("ตกลง",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							}).show();
		}

	}

	@Override
	public void onBackPressed() {
		AnimateFirstDisplayListener.displayedImages.clear();
		super.onBackPressed();
	}

	class ItemAdapter extends BaseAdapter {

		private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

		private class ViewHolder {
			public TextView text;
			public TextView desc;
			public ImageView image;
		}

		@Override
		public int getCount() {
			if (imageUrls == null) {
				return 0;
			}
			return imageUrls.length;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;
			if (convertView == null) {
				view = getLayoutInflater().inflate(R.layout.item_list_image,
						parent, false);
				holder = new ViewHolder();
				holder.text = (TextView) view.findViewById(R.id.text);
				holder.desc = (TextView) view.findViewById(R.id.desc);
				holder.image = (ImageView) view.findViewById(R.id.image);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}
			if (titles != null) {
				holder.text.setText(titles[position]);
				holder.desc.setText(descs[position]);
				imageLoader.displayImage(imageUrls[position], holder.image,
						options, animateFirstListener);
			}
			return view;
		}
	}

	private static class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

	@Override
	public void prepareImages(ArrayList<String> listImg) {
		IMAGES = new String[listImg.size()];
		for (int i = 0; i < listImg.size(); i++) {
			IMAGES[i] = (String) listImg.get(i);
		}
		Intent intent = new Intent(this, /*ImagePagerActivity*/ViewPagerActivity.class);
		intent.putExtra(Extra.IMAGES, IMAGES);
		startActivity(intent);
	}

	@Override
	public void listHitUpdate(ArrayList<HashMap<String, String>> hitList,
			ArrayList<HashMap<String, String>> updateList) {
		// TODO Auto-generated method stub

		MangaChapterActivity.chapterList = updateList;
		HomeActivity.listUpdatePage.put(
				"CHAP"
						+ MangaListActivity
								.getKeyFromUrl(MangaChapterActivity.url_manga)
						+ 1, updateList);

		Intent intent = new Intent(this, MangaChapterActivity.class);
		intent.putExtra(MangaChapterActivity.URL,
				MangaChapterActivity.url_manga);
		startActivity(intent);
	}

	// private void prepareCartoon(String key, String url) {
	// ArrayList<String> listImg = HomeActivity.allImage.get(key);
	// if (listImg == null || listImg.size() == 0) {
	// // must start tread to request html then parsing
	// httpParse = new MangaHttpParse(url,
	// MangaHttpParse.PARSE_ALL_UPDATE, this, this);
	// httpParse.start();
	// } else {
	// prepareImages(listImg);
	// }
	// }
}