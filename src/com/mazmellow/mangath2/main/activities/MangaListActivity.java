/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.mazmellow.mangath2.main.activities;

import static com.mazmellow.mangath2.uil.Constants.IMAGES;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;
import buzzcity.android.sdk.BCAdsClientBanner;

import com.mazmellow.mangath2.R;
import com.mazmellow.mangath2.main.DatabaseManager;
import com.mazmellow.mangath2.main.MangaHttpParse;
import com.mazmellow.mangath2.main.MangaHttpParse.MangaHttpParseListenner;
import com.mazmellow.mangath2.main.RequestHttpClient;
import com.mazmellow.mangath2.main.RequestHttpClient.RequestHttpClientListenner;
import com.mazmellow.mangath2.uil.AbsListViewBaseActivity;
import com.mazmellow.mangath2.uil.Constants.Extra;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.searchboxsdk.android.StartAppSearch;

/**
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 */
public class MangaListActivity extends AbsListViewBaseActivity implements
		MangaHttpParseListenner {

	DisplayImageOptions options;
	public static ArrayList<HashMap<String, String>> mangaList;
	ArrayList<HashMap<String, String>> usedList;

	String[] imageUrls;
	String[] titles;
	String[] urls;
	String[] descs;
	String[] tags;

	public static int page = 1;
	boolean openChapter;

	MangaHttpParse httpParse;
	// ImageAdapter adapter;
	ItemAdapter adapter;

	// TextView pageTextView;
	DatabaseManager myDb;
	ArrayList<HashMap<String, String>> bookmarkList;

	EditText editText;
	Button btnSearch;
//	boolean lastHasKeyword = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ac_image_list4);
		StartAppSearch.showSearchBox(this);
		
		usedList = mangaList;

		editText = (EditText) findViewById(R.id.editText1);
		btnSearch = (Button) findViewById(R.id.btnSearch);
		btnSearch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				if (lastHasKeyword) {
//					usedList = mangaList;
//					lastHasKeyword = false;
//					btnSearch.setText("ค้นหา");
//					editText.setText("");
//				} else {
					String keyword = editText.getText().toString();
					System.out.println("keyword = " + keyword);
					if (keyword == null || keyword.equals("")) {
						usedList = mangaList;
//						lastHasKeyword = false;
//						btnSearch.setText("ค้นหา");
					} else {
						filterUsedList(keyword);
//						lastHasKeyword = true;
//						btnSearch.setText("เคลียร์");
					}
//				}
				refreshPage();
			}
		});
		
		Button btnClearText = (Button)findViewById(R.id.btnClearText);
		btnClearText.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				editText.setText("");
			}
		});

		imageUrls = new String[usedList.size()];
		titles = new String[usedList.size()];
		descs = new String[usedList.size()];
		urls = new String[usedList.size()];
		tags = new String[usedList.size()];
		for (int i = 0; i < usedList.size(); i++) {
			HashMap<String, String> hash = usedList.get(i);
			titles[i] = (String) hash.get(MangaHttpParse.MANGA_TITLE);
			descs[i] = (String) hash.get(MangaHttpParse.MANGA_DESC);
			imageUrls[i] = (String) hash.get(MangaHttpParse.MANGA_IMG);
			urls[i] = (String) hash.get(MangaHttpParse.MANGA_URL);
			tags[i] = (String) hash.get(MangaHttpParse.MANGA_TAG);

			Log.i("title", titles[i]);
			Log.i("descs", descs[i]);
			Log.i("imageUrls", imageUrls[i]);
			Log.i("urls", urls[i]);
			Log.i("tags", tags[i]);
			System.out.println("----------------------------");
		}

		options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_stub)
				.showImageForEmptyUri(R.drawable.ic_empty)
				.showImageOnFail(R.drawable.ic_error).cacheInMemory(true)
				.cacheOnDisc(true).displayer(new RoundedBitmapDisplayer(20))
				.build();

		// listView = (GridView) findViewById(R.id.gridview);
		// adapter = new ImageAdapter();
		// ((GridView) listView).setAdapter(adapter);
		// listView.setOnItemClickListener(new OnItemClickListener() {
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view,
		// int position, long id) {
		// openManga(imageUrls[position], urls[position]);
		// }
		// });

		listView = (ListView) findViewById(android.R.id.list);
		adapter = new ItemAdapter();
		((ListView) listView).setAdapter(adapter);
		// listView.setOnItemClickListener(new OnItemClickListener() {
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view,
		// int position, long id) {
		// ImagePagerActivity.mangaSel = titles[position]
		// + descs[position];
		// openManga(imageUrls[position], urls[position]);
		// }
		// });
		// listView.setOnItemLongClickListener(new
		// AdapterView.OnItemLongClickListener() {
		//
		// @Override
		// public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
		// int position, long id) {
		// // TODO Auto-generated method stub
		// showOpenManga(titles[position], descs[position],
		// urls[position], imageUrls[position]);
		// return false;
		// }
		// });
		myDb = new DatabaseManager(this);

		refreshBookmark();

		// pageTextView = (TextView) findViewById(R.id.textView3);
		// pageTextView.setText("หน้าที่ " + page);

		BCAdsClientBanner graphicAdClient = new BCAdsClientBanner(106400,
				BCAdsClientBanner.ADTYPE_MWEB,
				BCAdsClientBanner.IMGSIZE_MWEB_216x36, this);
		ImageView graphicalAds = (ImageView) findViewById(R.id.ads4);
		graphicAdClient.getGraphicalAd(graphicalAds);
		
		showTipAddTag(this);

	}
	
	static SharedPreferences sharedPref;
	public static final String SHOW_TIP_MANGA_ADDTAG = "show_tip_manga_addtag";

	public static void showTipAddTag(Context context) {
		if (sharedPref == null) {
			sharedPref = context.getSharedPreferences(
					context.getString(R.string.preference_file_key),
					Context.MODE_PRIVATE);
		}
		boolean isShow = sharedPref.getBoolean(SHOW_TIP_MANGA_ADDTAG, false);
		Log.i("isShow", isShow + "");
		if (!isShow) {
			// not save >> show
			View checkBoxView = View.inflate(context, R.layout.checkbox, null);
			CheckBox checkBox = (CheckBox) checkBoxView
					.findViewById(R.id.checkbox);
			checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {

					// Save to shared preferences
					SharedPreferences.Editor editor = sharedPref.edit();
					editor.putBoolean(SHOW_TIP_MANGA_ADDTAG, true);
					editor.commit();
				}
			});
			checkBox.setText("ไม่แสดงข้อความนี้อีก");

			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle("Tips");
			builder.setMessage(
					"ตอนนี้คุณสามารถค้นหาการ์ตูนได้แล้ว มาช่วยกันเพิ่มข้อความค้นหาของแต่ละการ์ตูน โดยการกดค้างที่ลิสต์กันนะคะ")
					.setView(checkBoxView)
					.setCancelable(false)
					.setPositiveButton("ตกลง",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							}).show();
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (listView != null) {
			listView.requestFocus();
			refreshPage();
		}
	}

	protected void filterUsedList(String keyword) {
		// TODO Auto-generated method stub
		keyword = keyword.toLowerCase();
		ArrayList<HashMap<String, String>> filterList = new ArrayList<HashMap<String, String>>();
		for (int i = 0; i < mangaList.size(); i++) {
			HashMap<String, String> hash = mangaList.get(i);
			String title = (String) hash.get(MangaHttpParse.MANGA_TITLE);
			String tag = (String) hash.get(MangaHttpParse.MANGA_TAG);

			title = title.toLowerCase();
			if (tag != null) {
				tag = tag.toLowerCase();
				if (title.indexOf(keyword) != -1 || tag.indexOf(keyword) != -1) {
					filterList.add(hash);
				}
			} else {
				if (title.indexOf(keyword) != -1) {
					filterList.add(hash);
				}
			}

		}
		usedList = filterList;
	}

	void refreshPage() {
		imageUrls = new String[usedList.size()];
		titles = new String[usedList.size()];
		descs = new String[usedList.size()];
		urls = new String[usedList.size()];
		tags = new String[usedList.size()];
		for (int i = 0; i < usedList.size(); i++) {
			HashMap<String, String> hash = usedList.get(i);
			titles[i] = (String) hash.get(MangaHttpParse.MANGA_TITLE);
			descs[i] = (String) hash.get(MangaHttpParse.MANGA_DESC);
			imageUrls[i] = (String) hash.get(MangaHttpParse.MANGA_IMG);
			urls[i] = (String) hash.get(MangaHttpParse.MANGA_URL);
			tags[i] = (String) hash.get(MangaHttpParse.MANGA_TAG);

			Log.i("title", titles[i]);
			Log.i("descs", descs[i]);
			Log.i("imageUrls", imageUrls[i]);
			Log.i("urls", urls[i]);
			Log.i("tags", tags[i]);
			System.out.println("----------------------------");
		}
		refreshBookmark();

		// hide keyboard
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
	}

	public void refreshBookmark() {
		// fetch database
		if (myDb != null) {
			bookmarkList = myDb.getAllBookmark();
			if (bookmarkList == null) {
				bookmarkList = new ArrayList<HashMap<String, String>>();
			}
		}
		if (adapter != null) {
			adapter.notifyDataSetChanged();
		}
	}

	protected void showOpenManga(final String title, final String desc,
			final String url, final String image) {
		// TODO Auto-generated method stub

		// http://www.cartoonclub-th.com/one-piece/721/
		int lastIndex = url.indexOf("/", url.indexOf(".com") + 5);
		final String newUrl = url.substring(0, lastIndex + 1);

		// create a Dialog component
		final Dialog dialog = new Dialog(this);

		// tell the Dialog to use the dialog.xml as it's layout description
		dialog.setContentView(R.layout.dialog);
		dialog.setTitle(title);

		// TextView txt = (TextView) dialog.findViewById(R.id.tvTipName);
		// txt.setText("This is an Android custom Dialog Box Example! Enjoy!");

		Button gotoButton = (Button) dialog.findViewById(R.id.btnGoto);
		gotoButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImagePagerActivity.mangaSel = title + desc;
				openManga(image, newUrl);
				dialog.dismiss();
			}
		});

		Button bookmarkButton = (Button) dialog.findViewById(R.id.btnBookmark);
		bookmarkButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				onBookmark(title, "", newUrl, image);
				dialog.dismiss();
			}
		});

		Button unReadButton = (Button) dialog.findViewById(R.id.btnUnRead);
		unReadButton.setTextColor(0xFF888888);
		unReadButton.setEnabled(false);
		// unReadButton.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// System.out.println("url: "+url);
		// HomeActivity.unReadChapterManga(url);
		// adapter.notifyDataSetChanged();
		// dialog.dismiss();
		// }
		// });

		Button addTagButton = (Button) dialog.findViewById(R.id.btnAddTag);
		addTagButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				showDialogAddTag(title);
			}
		});

		Button cancelButton = (Button) dialog.findViewById(R.id.btnCancel);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	protected void showDialogAddTag(final String title) {
		// TODO Auto-generated method stub
		final Dialog dialog = new Dialog(this);

		// tell the Dialog to use the dialog.xml as it's layout description
		dialog.setContentView(R.layout.dialog_addtag);
		dialog.setTitle("ร่วมเพิ่มคำค้นหาให้การ์ตูน " + title);
		// TextView addTagTitle =
		// (TextView)dialog.findViewById(R.id.addTagTitle);
		// addTagTitle.setText("ร่วมช่วยกันเพิ่มคำค้นหาให้กับการ์ตูน "+title+" กันนะคะ");

		final EditText editText2 = (EditText) dialog
				.findViewById(R.id.editText2);

		Button okButton = (Button) dialog.findViewById(R.id.btnAddTagOk);
		okButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String keyword = editText2.getEditableText().toString();
				keyword = keyword.trim();
				try {
					keyword = URLEncoder.encode(keyword, "utf-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("keyword = " + keyword);
				if (keyword != null && !keyword.equals("")) {
					requestAddTag(title, keyword);
					dialog.dismiss();
				}
			}
		});

		Button cancelButton = (Button) dialog
				.findViewById(R.id.btnAddTagCancel);
		cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	protected void requestAddTag(String title, String keyword) {
		// TODO Auto-generated method stub
		final String backUpTitle = title;
		try {
			title = URLEncoder.encode(title, "utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String url = "http://www.mazmellow.com/readmangath/addTagManga.php?title=" + title
				+ "&tag=" + keyword;
		RequestHttpClient httpClient = new RequestHttpClient(url,
				new RequestHttpClientListenner() {
					@Override
					public void onRequestStringCallback(String response) {
						// TODO Auto-generated method stub
						ArrayList<HashMap<String, String>> updateList = new ArrayList<HashMap<String, String>>();

						try {
							JSONArray jsonArray = new JSONArray(response);

							if (jsonArray != null && jsonArray.length() > 0) {

								for (int i = 0; i < jsonArray.length(); i++) {
									HashMap<String, String> updateMangaHash = new HashMap<String, String>();

									JSONObject jsonObj = jsonArray
											.getJSONObject(i);
									// if (jsonObj.has("mid"))
									// updateMangaHash.put(MANGA_MID,
									// jsonObj.getString("mid"));
									if (jsonObj.has("title"))
										updateMangaHash.put(MangaHttpParse.MANGA_TITLE,
												jsonObj.getString("title"));
									if (jsonObj.has("tag"))
										updateMangaHash.put(MangaHttpParse.MANGA_TAG,
												jsonObj.getString("tag"));
									if (jsonObj.has("img"))
										updateMangaHash.put(MangaHttpParse.MANGA_IMG,
												jsonObj.getString("img"));
									if (jsonObj.has("url"))
										updateMangaHash.put(MangaHttpParse.MANGA_URL,
												jsonObj.getString("url"));

									updateMangaHash.put(MangaHttpParse.MANGA_DESC, "");
									updateList.add(updateMangaHash);

								}
							}

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.out.println("error = " + e.getMessage());
						}

						mangaList = updateList;
						usedList = mangaList;
						refreshPage();
						
						Toast.makeText(MangaListActivity.this,
								"ขอบคุณสำหรับการเพิ่มคำค้นหาการ์ตูน " + backUpTitle + " มากนะคะ",
								Toast.LENGTH_LONG).show();
					}
				}, this);
		httpClient.start();
	}

	public void onBookmark(String title, String desc, String url, String img) {
		// save by title & url
		String head = img.substring(0, img.lastIndexOf("_") + 1);
		head += "200x0";
		head += img.substring(img.lastIndexOf("."));
		img = head;

		// if(!url.endsWith("?all")){
		// url += "?all";
		// }

		Log.i("title", title);
		Log.i("desc", desc);
		Log.i("img", img);
		Log.i("url", url);

		if (myDb.insertOrUpdateBookmark(title, desc, img, url)) {
			Toast.makeText(MangaListActivity.this,
					"เพิ่มการ์ตูน " + title + " ไปยังรายการโปรดสำเร็จแล้วค่ะ",
					Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(
					MangaListActivity.this,
					"ขออภัยค่ะ เพิ่มการ์ตูน " + title
							+ " ไปยังรายการโปรดไม่สำเร็จ โปรดลองอีกครั้งค่ะ",
					Toast.LENGTH_LONG).show();
		}
	}

	public static String getKeyFromUrl(String url) {
		// http://www.cartoonclub-th.com/baka-to-boin/
		String key = url.substring(url.indexOf(".com/") + 5).trim();
		return key;
	}

	protected void openManga(String img, String url) {
		// TODO Auto-generated method stub
		openChapter = true;
		MangaChapterActivity.url_manga = url;
		MangaChapterActivity.page = 1;
		ArrayList<HashMap<String, String>> list = HomeActivity.listUpdatePage
				.get("CHAP" + getKeyFromUrl(MangaChapterActivity.url_manga) + 1);
		if (list != null && list.size() > 0) {
			listHitUpdate(null, list);
		} else {
			httpParse = new MangaHttpParse(url,
					MangaHttpParse.PARSE_LIST_CHAPTER, this, this);
			httpParse.start();
		}
	}

	@Override
	public void onBackPressed() {
		AnimateFirstDisplayListener.displayedImages.clear();
		super.onBackPressed();
	}

	private void refreshList() {
		openChapter = false;
		ArrayList<HashMap<String, String>> list = HomeActivity.listUpdatePage
				.get("LIST" + page);
		if (list != null && list.size() > 0) {
			listHitUpdate(null, list);
		} else {
			String url = "http://www.cartoonclub-th.com/manga-list/all/any/name-az/"
					+ page;
			httpParse = new MangaHttpParse(url,
					MangaHttpParse.PARSE_LIST_MANGA, this, this);
			httpParse.start();
		}

	}

	public void onClickPrevious(View view) {
		if (page > 1) {
			page--;
			refreshList();
		}
	}

	public void onClickNext(View view) {
		page++;
		refreshList();
	}

	// public class ImageAdapter extends BaseAdapter {
	// @Override
	// public int getCount() {
	// return imageUrls.length;
	// }
	//
	// @Override
	// public Object getItem(int position) {
	// return null;
	// }
	//
	// @Override
	// public long getItemId(int position) {
	// return position;
	// }
	//
	// @Override
	// public View getView(int position, View convertView, ViewGroup parent) {
	// final ImageView imageView;
	// if (convertView == null) {
	// imageView = (ImageView) getLayoutInflater().inflate(
	// R.layout.item_grid_image, parent, false);
	// } else {
	// imageView = (ImageView) convertView;
	// }
	//
	// imageLoader.displayImage(imageUrls[position], imageView, options);
	//
	// return imageView;
	// }
	// }

	class ItemAdapter extends BaseAdapter {

		private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

		private class ViewHolder {
			public TextView text;
			public TextView desc;
			public ImageView image;
			public Button starBtn;
		}

		@Override
		public int getCount() {
			return imageUrls.length;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;
			if (convertView == null) {
				view = getLayoutInflater().inflate(R.layout.item_list_manga,
						parent, false);
				holder = new ViewHolder();
				holder.text = (TextView) view.findViewById(R.id.text);
				holder.desc = (TextView) view.findViewById(R.id.desc);
				holder.image = (ImageView) view.findViewById(R.id.image);
				holder.starBtn = (Button) view.findViewById(R.id.starBtn);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			holder.text.setText(titles[position]);
			holder.desc.setText(descs[position]);

			if (checkBookmark(titles[position])) {
				holder.starBtn.setBackgroundResource(R.drawable.icon_star);
			} else {
				holder.starBtn.setBackgroundResource(R.drawable.icon_star_gray);
			}

			holder.starBtn.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					HashMap<String, String> bookmark = myDb
							.getBookmarkByTitle(titles[position]);
					if (bookmark == null) {
						myDb.insertOrUpdateBookmark(titles[position],
								descs[position], imageUrls[position],
								urls[position]);
					} else {
						myDb.deleteBookmarkByTitle(titles[position]);
					}
					refreshBookmark();
				}
			});

			imageLoader.displayImage(imageUrls[position], holder.image,
					options, animateFirstListener);

			view.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					ImagePagerActivity.mangaSel = titles[position]
							+ descs[position];
					openManga(imageUrls[position], urls[position]);
				}

			});

			view.setOnLongClickListener(new View.OnLongClickListener() {

				@Override
				public boolean onLongClick(View v) {
					// TODO Auto-generated method stub
					showOpenManga(titles[position], descs[position],
							urls[position], imageUrls[position]);
					return false;
				}
			});

			return view;
		}
	}

	public boolean checkBookmark(String _title) {
		// TODO Auto-generated method stub
		boolean isBookmark = false;
		if (bookmarkList != null) {
			for (int i = 0; i < bookmarkList.size(); i++) {
				HashMap<String, String> hash = bookmarkList.get(i);
				String title = (String) hash.get(MangaHttpParse.MANGA_TITLE);
				if (title.equalsIgnoreCase(_title)) {
					isBookmark = true;
					break;
				}
			}
		}
		return isBookmark;
	}

	private static class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

	@Override
	public void prepareImages(ArrayList<String> listImg) {
		IMAGES = new String[listImg.size()];
		for (int i = 0; i < listImg.size(); i++) {
			IMAGES[i] = (String) listImg.get(i);
		}
		Intent intent = new Intent(this, ImagePagerActivity.class);
		intent.putExtra(Extra.IMAGES, IMAGES);
		startActivity(intent);
	}

	@Override
	public void listHitUpdate(ArrayList<HashMap<String, String>> hitList,
			ArrayList<HashMap<String, String>> updateList) {
		// TODO Auto-generated method stub

		if (!openChapter) {
			MangaUpdateActivity.updateList = updateList;
			HomeActivity.listUpdatePage.put("LIST" + page, updateList);

			imageUrls = new String[updateList.size()];
			titles = new String[updateList.size()];
			descs = new String[updateList.size()];
			urls = new String[updateList.size()];
			tags = new String[updateList.size()];
			for (int i = 0; i < updateList.size(); i++) {
				HashMap<String, String> hash = updateList.get(i);
				titles[i] = (String) hash.get(MangaHttpParse.MANGA_TITLE);
				descs[i] = (String) hash.get(MangaHttpParse.MANGA_DESC);
				imageUrls[i] = (String) hash.get(MangaHttpParse.MANGA_IMG);
				urls[i] = (String) hash.get(MangaHttpParse.MANGA_URL);
				tags[i] = (String) hash.get(MangaHttpParse.MANGA_TAG);

				Log.i("title", titles[i]);
				Log.i("descs", descs[i]);
				Log.i("imageUrls", imageUrls[i]);
				Log.i("urls", urls[i]);
				Log.i("tags", tags[i]);
				System.out.println("----------------------------");
			}

			adapter.notifyDataSetChanged();
			// pageTextView.setText("หน้าที่ " + page);

			listView.setSelection(0);
		} else {
			MangaChapterActivity.chapterList = updateList;
			HomeActivity.listUpdatePage.put("CHAP"
					+ getKeyFromUrl(MangaChapterActivity.url_manga) + 1,
					updateList);

			Intent intent = new Intent(this, MangaChapterActivity.class);
			intent.putExtra(MangaChapterActivity.URL,
					MangaChapterActivity.url_manga);
			startActivity(intent);
		}

	}

}