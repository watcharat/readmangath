package com.mazmellow.mangath2.main;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.mazmellow.mangath2.main.RequestHttpClient.RequestHttpClientListenner;
import com.mazmellow.mangath2.main.activities.HomeActivity;
import com.mazmellow.mangath2.main.activities.ImagePagerActivity;
//import android.util.Log;

public class MangaHttpParse implements RequestHttpClientListenner {

	private int nowParse;
	public static final int PARSE_FIRST = 0;
//	public static final int PARSE_ALL = 1;
	public static final int PARSE_ALL_UPDATE = 2;
	public static final int PARSE_UPDATE_LIST = 3;
	public static final int PARSE_LIST_MANGA = 4;
	public static final int PARSE_LIST_CHAPTER = 5;
	public static final int PARSE_LIST_MANGA2 = 6;

	// key hash manga
	public static final String MANGA_TITLE = "title";
	public static final String MANGA_DESC = "desc";
	public static final String MANGA_IMG = "img";
	public static final String MANGA_URL = "url";
	public static final String MANGA_TAG = "tag";
//	public static final String MANGA_MID = "mid";

	RequestHttpClient client;
	MangaHttpParseListenner listenner;
	Context activity;

	public MangaHttpParse(String url, int parseStyle,
			MangaHttpParseListenner _listenner, Context _activity) {
		// TODO Auto-generated constructor stub
		nowParse = parseStyle;
		listenner = _listenner;
		activity = _activity;

		client = new RequestHttpClient(url, this, activity);
	}

	public void start() {
		if (client != null) {
			client.start();
		}
	}

	@Override
	public void onRequestStringCallback(String response) {
		// TODO Auto-generated method stub
		// //System.out.println("response = "+response);

		try {
			/*if (nowParse == PARSE_ALL) {

				response = response.substring(response
						.indexOf("wpm_pag mng_rdr"));
				response = response
						.substring(response.indexOf("</center>") + 8);
				response = response.substring(0, response.indexOf("script"));

				ArrayList<String> links = new ArrayList<String>();
				while (response.indexOf("img src=") != -1) {
					//System.out.println("response = " + response);
					String link = response.substring(
							response.indexOf("src=") + 5,
							response.indexOf("</center") - 2);
					// //System.out.println("link = " + link);
					if(link.endsWith("\"")){
						link = link.substring(0,link.length()-1);
					}
					links.add(link);
					response = response
							.substring(response.indexOf("</center>") + 8);
				}

				// save
				HomeActivity.allImage.put(ImagePagerActivity.mangaSel, links);

				if (listenner != null) {
					listenner.prepareImages(links);
				}
			} else*/ if (nowParse == PARSE_ALL_UPDATE) {

				// response =
				// response.substring(response.indexOf("wpm_pag mng_rdr"));
				
				////System.out.println("response1 = "+response);

				ArrayList<String> links = new ArrayList<String>();
				while (response.indexOf("max-width") != -1) {
					
					String startKey = "max-width";
					response = response.substring(response
							.indexOf(startKey)+startKey.length());
					
					String key = "src=\"";
					response = response.substring(response
							.indexOf(key));
					
					////System.out.println("response2 = "+response);
					
					String keyCutLast = "";
					if(response.indexOf("\"  />")!=-1){
						keyCutLast = "\"  />";
					}else if(response.indexOf("\"/> <img")!=-1){
						keyCutLast = "\"/> <img";
					}else if(response.indexOf("\"/></center>")!=-1){
						keyCutLast = "\"/></center>";
					}else if(response.indexOf("\" />")!=-1){
						keyCutLast = "\" />";
					}
					
					String link = response.substring(
							response.indexOf(key) + key.length(),
							response.indexOf(keyCutLast));
					////System.out.println("link = " + link);
					if (link.endsWith("\"")) {
						link = link.substring(0, link.length() - 1);
					}
//					//Log.i("IMAGE", link);
					
					if(checkUrl(link)){
						links.add(link);
					}
					
					
					if(response.indexOf(startKey)>-1){
						response = response.substring(response
							.indexOf(startKey));
					}
				}

				// save
				HomeActivity.allImage.put(ImagePagerActivity.mangaSel, links);

				if (listenner != null) {
					listenner.prepareImages(links);
				}
			}

			else if (nowParse == PARSE_FIRST) {
				// get all hit manga and update manga
				ArrayList<HashMap<String, String>> hitList = new ArrayList<HashMap<String, String>>();
				ArrayList<HashMap<String, String>> updateList = new ArrayList<HashMap<String, String>>();
				
				
				System.out.println("LENGHT = "+response.length());
				
				// start get hit manga
				response = response.substring(response
						.indexOf("<h2>อัพเดทการ์ตูนยอดนิยม</h2>"));

				// start get update manga
				response = response
						.substring(response
								.indexOf("sct_content"));
				while (response.indexOf("class=\"row\">") != -1) {

					String keyCut = "class=\"row\">";
					// find next
					int nextIndex = response.indexOf(keyCut,
							response.indexOf(keyCut.substring(1)));
					if (nextIndex == -1) {
						String keyCut2 = "wpm_nav\">";
						nextIndex = response.indexOf(keyCut2);
					}
					// sub next
					String updateText = response.substring(
							response.indexOf(keyCut), nextIndex);
					// //System.out.println("updateText = "+updateText);
					response = response.substring(nextIndex);

					String startIndex = "";
					String endIndex = "";
					int countBack = 0;

					startIndex = "href=\"";
					endIndex = "\" title";
					countBack = 0;
					updateText = updateText.substring(updateText
							.indexOf(startIndex));
					String url = updateText.substring(
							updateText.indexOf(startIndex)
									+ startIndex.length(),
							updateText.indexOf(endIndex) - countBack);

					startIndex = "src=\"";
					endIndex = "alt=\"";
					countBack = 2;
					updateText = updateText.substring(updateText
							.indexOf(startIndex));
					String img = updateText.substring(
							updateText.indexOf(startIndex)
									+ startIndex.length(),
							updateText.indexOf(endIndex) - countBack);
					if (img.endsWith("36x0.png")) {
						img = img.replaceAll("36x0.png", "200x0.png");
					}
					if (img.endsWith("36x0.jpg")) {
						img = img.replaceAll("36x0.jpg", "200x0.jpg");
					}
					//Log.i(MANGA_IMG, img);

					startIndex = "alt=\"";
					endIndex = ">";
					countBack = 2;
					updateText = updateText.substring(updateText
							.indexOf(startIndex));
					String title = updateText.substring(
							updateText.indexOf(startIndex)
									+ startIndex.length(),
							updateText.indexOf(endIndex) - countBack);

					String desc = "";

					while (updateText.indexOf("<li>") != -1) {
						
						updateText = updateText.substring(updateText
								.indexOf("<li>"));
						
						HashMap<String, String> updateMangaHash = new HashMap<String, String>();
						// listManga = new HashMap<String, String>();

						////System.out.println("updateText = "+updateText);

						startIndex = "href=\"";
						endIndex = "title";
						countBack = 2;
						updateText = updateText.substring(updateText
								.indexOf(startIndex));
						url = updateText.substring(
								updateText.indexOf(startIndex)
										+ startIndex.length(),
								updateText.indexOf(endIndex) - countBack);
						if (url.endsWith("\"")) {
							url = url.substring(0, url.length() - 1);
						}
						//System.out.println("url = "+url);
						
						//url += "?all";

						startIndex = "lng_\">";
						endIndex = "</b>";
						countBack = 0;
						updateText = updateText.substring(updateText
								.indexOf(startIndex));
						desc = updateText.substring(
								updateText.indexOf(startIndex)
										+ startIndex.length(),
								updateText.indexOf(endIndex) - countBack);
						
			
						
						String tag = "";
						
						updateMangaHash.put(MANGA_TITLE, title);
						updateMangaHash.put(MANGA_DESC, desc);
						updateMangaHash.put(MANGA_IMG, img);
						updateMangaHash.put(MANGA_URL, url);
						updateMangaHash.put(MANGA_TAG, tag);
						if(checkUrl(url)){
							updateList.add(updateMangaHash);
						}
						
						// updateMangaList.add(listManga);
					}
					// updateMangaHash.put(MANGA_LIST, updateMangaList);

					//System.out.println("---------------------");
				}
				
				/*
				// start get hit manga
				response = response.substring(response
						.indexOf("<div id=\"wpm_wgt_mng_lst-2\""));
				
				while(response.indexOf("<li>")!=-1){
					HashMap<String, String> hitMangaHash = new HashMap<String, String>();

					String startIndex = "";
					String endIndex = "";
					int countBack = 0;

					startIndex = "src=\"";
					endIndex = "\" alt=";
					countBack = 0;
					response = response.substring(response.indexOf(startIndex));

					String img = response.substring(
							response.indexOf(startIndex) + startIndex.length(),
							response.indexOf(endIndex) - countBack);
					String head = img.substring(0, img.lastIndexOf("_") + 1);
					head += "200x0";
					head += img.substring(img.lastIndexOf("."));
					img = head;
					
					
					startIndex = "<b>";
					endIndex = "</b>";
					countBack = 0;
					
					response = response.substring(response.indexOf(startIndex));
					String tag = response.substring(
							response.indexOf(startIndex) + startIndex.length(),
							response.indexOf(endIndex) - countBack);
					
					if(tag==null){
						tag = "";
					}

					startIndex = "title=\"";
					endIndex = "\" rel=\"";
					countBack = 0;
					
					response = response.substring(response.indexOf(startIndex));
					String title = response.substring(
							response.indexOf(startIndex) + startIndex.length(),
							response.indexOf(endIndex) - countBack);
					
					
					startIndex = "href=\"";
					endIndex = "title=\"";
					countBack = 3;
					
					////System.out.println("response3 = "+response);
					
					response = response.substring(response.indexOf(startIndex));
					String url = response.substring(
							response.indexOf(startIndex) + startIndex.length(),
							response.indexOf(endIndex) - countBack);
					if (url.endsWith("\"")) {
						url = url.substring(0, url.length() - 1);
					}
					// url += "all?all";
					//url += "?all";

					startIndex = "title=\"";
					endIndex = "\">";
					countBack = 0;
					response = response.substring(response.indexOf(startIndex));
					String desc = response.substring(
							response.indexOf(startIndex) + startIndex.length(),
							response.indexOf(endIndex) - countBack);

					//System.out.println("---------------------");
					//Log.i(MANGA_IMG, img);
					//Log.i(MANGA_TAG, tag);
					//Log.i(MANGA_URL, url);
					//Log.i(MANGA_TITLE, title);
					//Log.i(MANGA_DESC, desc);
					//System.out.println("---------------------");

					hitMangaHash.put(MANGA_TITLE, title);
					hitMangaHash.put(MANGA_DESC, desc);
					hitMangaHash.put(MANGA_IMG, img);
					hitMangaHash.put(MANGA_URL, url);
					hitMangaHash.put(MANGA_TAG, tag);

					if(checkUrl(url)){
						hitList.add(hitMangaHash);
					}	

					
				}*/
				// end get hit manga

				if (listenner != null) {
					listenner.listHitUpdate(hitList, updateList);
				}
			}

			else if (nowParse == PARSE_UPDATE_LIST) {

				ArrayList<HashMap<String, String>> updateList = new ArrayList<HashMap<String, String>>();

				////System.out.println("response = "+response);
				
				// start get update manga
				response = response.substring(response
						.indexOf("wpm_pag"));
				while (response.indexOf("class=\"row\">") != -1) {

					// ArrayList<HashMap<String, String>> updateMangaList = new
					// ArrayList<HashMap<String,String>>();

					String keyCut = "class=\"row\">";
					// find next
					int nextIndex = response.indexOf(keyCut,
							response.indexOf(keyCut.substring(1)));
					if (nextIndex == -1) {
						String keyCut2 = "class=\"wpm_nav\">";
						nextIndex = response.indexOf(keyCut2);
					}
					// sub next
					String updateText = response.substring(
							response.indexOf(keyCut), nextIndex);
					// //System.out.println("updateText = "+updateText);
					response = response.substring(nextIndex);

					String startIndex = "";
					String endIndex = "";
					int countBack = 0;

					startIndex = "href=\"";
					endIndex = "\" title";
					countBack = 0;
					updateText = updateText.substring(updateText
							.indexOf(startIndex));
					String url = updateText.substring(
							updateText.indexOf(startIndex)
									+ startIndex.length(),
							updateText.indexOf(endIndex) - countBack);

					startIndex = "src=\"";
					endIndex = "alt=\"";
					countBack = 2;
					updateText = updateText.substring(updateText
							.indexOf(startIndex));
					String img = updateText.substring(
							updateText.indexOf(startIndex)
									+ startIndex.length(),
							updateText.indexOf(endIndex) - countBack);
					if (img.endsWith("36x0.png")) {
						img = img.replaceAll("36x0.png", "200x0.png");
					}
					if (img.endsWith("36x0.jpg")) {
						img = img.replaceAll("36x0.jpg", "200x0.jpg");
					}
					//Log.i(MANGA_IMG, img);

					startIndex = "alt=\"";
					endIndex = ">";
					countBack = 2;
					updateText = updateText.substring(updateText
							.indexOf(startIndex));
					String title = updateText.substring(
							updateText.indexOf(startIndex)
									+ startIndex.length(),
							updateText.indexOf(endIndex) - countBack);

					String desc = "";
//					if (updateText.indexOf("<b class=\"upd_sts\">New!</b>") != -1) {
//						desc = "New!";
//					}

					//System.out.println("---------------------");
					//Log.i("-----> LIST: " + MANGA_IMG, img);
					// //Log.i(MANGA_URL, url);
					// //Log.i(MANGA_TITLE, title);
					// //Log.i(MANGA_DESC, desc);

					// updateMangaHash.put(MANGA_TITLE, title);
					// updateMangaHash.put(MANGA_DESC, desc);
					// updateMangaHash.put(MANGA_IMG, img);
					// updateMangaHash.put(MANGA_URL, url);

					// HashMap<String, String> listManga;

					while (updateText.indexOf("<li>") != -1) {
						HashMap<String, String> updateMangaHash = new HashMap<String, String>();
						// listManga = new HashMap<String, String>();

						//Log.i("updateText", updateText);

						startIndex = "href=\"";
						endIndex = "title";
						countBack = 2;
						updateText = updateText.substring(updateText
								.indexOf(startIndex));
						url = updateText.substring(
								updateText.indexOf(startIndex)
										+ startIndex.length(),
								updateText.indexOf(endIndex) - countBack);
						if (url.endsWith("\"")) {
							url = url.substring(0, url.length() - 1);
						}
						//url += "?all";

						startIndex = "val lng_\">";
						endIndex = "</b>";
						countBack = 0;
						updateText = updateText.substring(updateText
								.indexOf(startIndex));
						desc = updateText.substring(
								updateText.indexOf(startIndex)
										+ startIndex.length(),
								updateText.indexOf(endIndex) - countBack);
	
//						updateText = updateText.substring(updateText
//								.indexOf("</li>"));
								
//						updateText = updateText.substring(updateText
//								.indexOf("class=\"dte\""));
//
//						startIndex = ">";
//						endIndex = "<";
//						countBack = 0;
//						updateText = updateText.substring(updateText
//								.indexOf(startIndex));
						// desc =
						// updateText.substring(updateText.indexOf(startIndex)
						// + startIndex.length(), updateText.indexOf(endIndex)
						// - countBack);

						//Log.i("LIST: " + MANGA_URL, url);
						//Log.i("LIST: " + MANGA_TITLE, title);
						//Log.i("LIST: " + MANGA_DESC, desc);

						// listManga.put(MANGA_URL, url);
						// listManga.put(MANGA_TITLE, title);
						// listManga.put(MANGA_DESC, desc);

						updateMangaHash.put(MANGA_TITLE, title);
						updateMangaHash.put(MANGA_DESC, desc);
						updateMangaHash.put(MANGA_IMG, img);
						updateMangaHash.put(MANGA_URL, url);

						if(checkUrl(url)){
							updateList.add(updateMangaHash);
						}
						
						// updateMangaList.add(listManga);
					}
					// updateMangaHash.put(MANGA_LIST, updateMangaList);

					//System.out.println("---------------------");
				}

				if (listenner != null) {
					//updateList.remove(updateList.size() - 1);
					listenner.listHitUpdate(null, updateList);
				}
			}

			else if (nowParse == PARSE_LIST_MANGA) {

				ArrayList<HashMap<String, String>> updateList = new ArrayList<HashMap<String, String>>();

				// start get update manga
				response = response.substring(response
						.indexOf("class=\"nde"));
				
				
				
				while (response.indexOf("class=\"nde") != -1) {
//					//Log.i("response", response);
					HashMap<String, String> updateMangaHash = new HashMap<String, String>();

					String startIndex = "";
					String endIndex = "";
					int countBack = 0;

					startIndex = "href=\"";
					endIndex = "\" title";
					countBack = 0;
					response = response.substring(response.indexOf(startIndex));
					String url = response.substring(
							response.indexOf(startIndex) + startIndex.length(),
							response.indexOf(endIndex) - countBack);
					
					startIndex = "title=\"";
					endIndex = "class";
					countBack = 2;
					response = response.substring(response.indexOf(startIndex));
					String title = response.substring(
							response.indexOf(startIndex) + startIndex.length(),
							response.indexOf(endIndex) - countBack);
					

					startIndex = "src=\"";
					endIndex = "alt=\"";
					countBack = 2;
					response = response.substring(response.indexOf(startIndex));
					String img = response.substring(
							response.indexOf(startIndex) + startIndex.length(),
							response.indexOf(endIndex) - countBack);
					String head = img.substring(0, img.lastIndexOf("_") + 1);
					head += "200x0";
					head += img.substring(img.lastIndexOf("."));
					img = head;
					//Log.i(MANGA_IMG, img);

//					startIndex = "alt=\"";
//					endIndex = "\"/>";
//					countBack = 0;
//					response = response.substring(response.indexOf(startIndex));
//					String desc = response.substring(
//							response.indexOf(startIndex) + startIndex.length(),
//							response.indexOf(endIndex) - countBack);
					String desc = "";

					//System.out.println("---------------------");
					//Log.i("LIST: " + MANGA_IMG, img);
					//Log.i("LIST: " + MANGA_URL, url);
					//Log.i("LIST: " + MANGA_TITLE, title);
					//Log.i("LIST: " + MANGA_DESC, desc);

					updateMangaHash.put(MANGA_TITLE, title);
					updateMangaHash.put(MANGA_DESC, desc);
					updateMangaHash.put(MANGA_IMG, img);
					updateMangaHash.put(MANGA_URL, url);
					
					if(checkUrl(url)){
						updateList.add(updateMangaHash);
					}
					
					//System.out.println("---------------------");
					
					if(response.indexOf("class=\"nde")>-1){
						response = response.substring(response.indexOf("class=\"nde"));
					}
				}

				if (listenner != null) {
					//updateList.remove(updateList.size() - 1);
					listenner.listHitUpdate(null, updateList);
				}
			}
			
			else if(nowParse == PARSE_LIST_MANGA2){
				
				ArrayList<HashMap<String, String>> updateList = new ArrayList<HashMap<String, String>>();
				
				try {
					JSONArray jsonArray = new JSONArray(response);

					if (jsonArray != null && jsonArray.length() > 0) {
						
						for (int i=0; i<jsonArray.length(); i++) {
							HashMap<String, String> updateMangaHash = new HashMap<String, String>();
							
							JSONObject jsonObj = jsonArray.getJSONObject(i);
//							if (jsonObj.has("mid"))
//								updateMangaHash.put(MANGA_MID, jsonObj.getString("mid"));
							if (jsonObj.has("title"))
								updateMangaHash.put(MANGA_TITLE, jsonObj.getString("title"));
							if (jsonObj.has("tag"))
								updateMangaHash.put(MANGA_TAG, jsonObj.getString("tag"));
							if (jsonObj.has("img"))
								updateMangaHash.put(MANGA_IMG, jsonObj.getString("img"));
							if (jsonObj.has("url"))
								updateMangaHash.put(MANGA_URL, jsonObj.getString("url"));
							
							updateMangaHash.put(MANGA_DESC, "");
							updateList.add(updateMangaHash);

						}
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("error = " + e.getMessage());
				}
				
				if (listenner != null) {
					//updateList.remove(updateList.size() - 1);
					listenner.listHitUpdate(null, updateList);
				}
				
			}

			else if (nowParse == PARSE_LIST_CHAPTER) {

				ArrayList<HashMap<String, String>> updateList = new ArrayList<HashMap<String, String>>();
				
				String startKey = "wpm_pag mng_det\">";
				
				if (response.indexOf(startKey) != -1) {
					
					response = response.substring(response.indexOf(startKey)+startKey.length());
					
					String startIndex = "#ccc;\">";
					String endIndex = "</b>";
					int countBack = 0;
					response = response.substring(response.indexOf(startIndex));
					String title = response.substring(
							response.indexOf(startIndex) + startIndex.length(),
							response.indexOf(endIndex) - countBack);
					
					response = response.substring(response.indexOf("src=")+5);

					startIndex = "src=\"";
					endIndex = "alt=";
					countBack = 2;
					response = response.substring(response.indexOf(startIndex));
					String img = response.substring(
							response.indexOf(startIndex) + startIndex.length(),
							response.indexOf(endIndex) - countBack);
					
					boolean skip = false;
					
					response = response.substring(response.indexOf("class=\"lst\""));

					while (response.indexOf("<li") != -1) {
						if(response.startsWith("</ul>")){
							break;
						}
						response = response.substring(response.indexOf("<li"));
						if(!skip){
							response = response.substring(response.indexOf("</li"));
							skip = true;
							continue;
						}
						if(response.indexOf("<li><a href")==0 || response.indexOf("href=\"http")==0){
							break;
						}

						HashMap<String, String> updateMangaHash = new HashMap<String, String>();

						//System.out.println("1");
						
						startIndex = "href=\"";
						endIndex = "title=";
						countBack = 2;
						response = response.substring(response
								.indexOf(startIndex));
						String url = response.substring(
								response.indexOf(startIndex)
										+ startIndex.length(),
								response.indexOf(endIndex) - countBack);
						//url += "?all";
						//System.out.println("2");
						
						try {
							response = response.substring(response
									.indexOf("text-align:center"));
						} catch (Exception e) {
							// TODO: handle exception
							break;
						}
						
						
						//System.out.println("3");

						startIndex = "\">";
						endIndex = "</b>";
						countBack = 0;
						response = response.substring(response
								.indexOf(startIndex));
						String desc = response.substring(
								response.indexOf(startIndex)
										+ startIndex.length(),
								response.indexOf(endIndex) - countBack);

						//System.out.println("---------------------");
						//Log.i("LIST: " + MANGA_IMG, img);
						//Log.i("LIST: " + MANGA_URL, url);
						//Log.i("LIST: " + MANGA_TITLE, title);
						//Log.i("LIST: " + MANGA_DESC, desc);

						updateMangaHash.put(MANGA_TITLE, title);
						updateMangaHash.put(MANGA_DESC, desc);
						updateMangaHash.put(MANGA_IMG, img);
						updateMangaHash.put(MANGA_URL, url);

						if(checkUrl(url)){
							updateList.add(updateMangaHash);
						}
						
						//System.out.println("---------------------");
						
						String keyEnd = "</li>";
						if(response.indexOf(keyEnd)!=-1){
							response = response.substring(response.indexOf(keyEnd)+keyEnd.length());
						}
					}
				}

				if (listenner != null) {
					listenner.listHitUpdate(null, updateList);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			//Log.i("ERROR PARSE_LIST_CHAPTER", e.getMessage());
		}

	}

	public static boolean checkUrl(String url) {
		// TODO Auto-generated method stub
		//System.out.println("checkUrl: "+url);
		boolean notFound = true;
		if(     
				//not have chapter
				url.indexOf("/a_bout/")!=-1 || 
				url.indexOf("/bakuman/")!=-1 ||
				url.indexOf("/dragon-ball-sd/")!=-1 ||
				url.indexOf("/immortal-regis/")!=-1 ||
				url.indexOf("/jaco_the_galactic_patrolman/")!=-1 ||
				url.indexOf("/kurohime/")!=-1 ||
				url.indexOf("/magico/")!=-1 ||
				url.indexOf("/Reinou-Tantei-Miko/")!=-1 ||
				url.indexOf("/shaman-king-remix-track/")!=-1 ||
				url.indexOf("/swot/")!=-1 ||
				
				//all chapter are X
				url.indexOf("/air-gear/")!=-1 ||
				url.indexOf("/avatar/")!=-1 ||
				url.indexOf("/baki-1-the-grappler/")!=-1 ||
				url.indexOf("/baki-in-search-of-our-strongest-hero/")!=-1 ||
				url.indexOf("/cage-of-eden/")!=-1 ||
				url.indexOf("/dragon-ball/ ")!=-1 ||
				url.indexOf("/final-fantasy-xii/")!=-1 ||
				url.indexOf("/godhand_teru/")!=-1 ||
				url.indexOf("/high-school-of-the-dead/")!=-1 ||
				url.indexOf("/prince-of-tennis/")!=-1 ||
				url.indexOf("/ruler_of_the_land/")!=-1 ||
				url.indexOf("/samurai-deeper-kyo/")!=-1 ||
				url.indexOf("/the-breaker/ ")!=-1 ||
				
				// some chapter
				url.indexOf("/asamiya-san-no-imouto/")!=-1 ||
				url.indexOf("/berserk/")!=-1 ||
				url.indexOf("/drifters/")!=-1 ||
				url.indexOf("/gantz/")!=-1 ||
				url.indexOf("/gintama/")!=-1 ||
				url.indexOf("/kokou-no-hito/")!=-1 ||
				url.indexOf("/magi/")!=-1 ||
				url.indexOf("/new-prince-of-tennis/")!=-1 ||
				url.indexOf("/noblesse/")!=-1 ||
				url.indexOf("/prunus-girl/")!=-1 ||
				url.indexOf("/psyren/")!=-1 ||
				url.indexOf("/shaman-king/")!=-1 ||
				url.indexOf("/the-world-god-only-knows/")!=-1
				
				
		){
			
			//System.out.println("--- SKIP ---");
			notFound = false;
		}
		
		return notFound;
		
	}

	// private String cutText(String startIndex, String endIndex, int countBack,
	// String text){
	// text = text.substring(text.indexOf(startIndex));
	// return
	// text.substring(text.indexOf(startIndex)+startIndex.length(),text.indexOf(endIndex)-countBack);
	// }

	public interface MangaHttpParseListenner {
		public void prepareImages(ArrayList<String> listImg);

		public void listHitUpdate(ArrayList<HashMap<String, String>> hitList,
				ArrayList<HashMap<String, String>> updateList);
	}
}
